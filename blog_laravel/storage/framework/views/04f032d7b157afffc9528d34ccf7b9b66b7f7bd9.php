 

<?php $__env->startSection('content'); ?>

        <!--main contents start-->
        <main class="main-content">
            <!--page title start-->
            <div class="page-title">
                <h4 class="mb-0"> Edit Data
                </h4>
                <ol class="breadcrumb mb-0 pl-0 pt-1 pb-0">
                    <!-- <li class="breadcrumb-item"><a href="#" class="default-color">Home</a></li>
                    <li class="breadcrumb-item active">Basic Input</li> -->
                </ol>
            </div>
            <!--page title end-->
            <div class="container-fluid">

                <!-- state start-->
                <div class="row">
                    <div class=" col-md-6">           
                        <div class="card card-shadow mb-4">
                           
                            <div class="card-body">
                                <form method="post" action="<?php echo e(route('menus.update')); ?>" >
                                    <?php echo e(csrf_field()); ?>

                                    
                                    <input type="hidden" name="id" value="<?php echo e($data->id); ?>">
                
                                    <div class="form-group">
                                        <label for="name"><?php echo e(__('Name')); ?></label>
                                        <input type="text" name="name" class="form-control form-control-pill" id="name" value="<?php echo e($data->name); ?>">
                                    </div>
                                   
                                     <div class="form-group">         
                                    <label for="delivery_type"><?php echo e(__('Status')); ?></label>&nbsp;
                                        <div class="col-sm-10">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input<?php echo e($errors->has('status') ? ' is-invalid' : ''); ?>" type="radio" name="status" value="1"  <?php if($data->status == 1 ): ?> <?php echo "checked"; ?> <?php endif; ?>>
                                                    Active
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input<?php echo e($errors->has('status') ? ' is-invalid' : ''); ?>" type="radio" name="status" value="0"   <?php if($data->status == 0  ): ?> <?php echo "checked"; ?> <?php endif; ?>>
                                                    In Active 
                                                </label>
                                            </div>
                                             <?php if($errors->has('status')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('status')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                        </div>
                                        </div>

                                    <button class="btn btn-lg btn-primary btn-block" name="edit" type="submit"><?php echo e(__('Edit')); ?></button>
                                </form>

                            </div>
                        </div>

                    </div>
                   
                </div>

                <!-- state end-->

            </div>
        </main>
        <!--main contents end-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('custom_js'); ?>
<script>
    $(document).ready(function() {
        $('#bs4-table').DataTable();
    } );
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>