<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Title Of Site -->
	<title>Thefoody - Order Online Portal Responsive HTML Template</title>
	<meta name="description" content="TheFoody is a food ordering platform which brings restaurants and food lovers together. Food ordering online is easier than any other platforms.">
	<meta name="keywords" content="food, order online, restaurant, reservation, book a table, foodies, cafe, recipes, menu, dishes, chefs and cooking experts ">
	<meta name="author" content="iglyphic">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Fav and Touch Icons -->
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo e(asset('public/assets/ico/apple-touch-icon-144-precomposed.png')); ?>">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo e(asset('public/assets/ico/apple-touch-icon-114-precomposed.png')); ?>">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo e(asset('public/assets/ico/apple-touch-icon-72-precomposed.png')); ?>">
	<link rel="apple-touch-icon-precomposed" href="<?php echo e(asset('public/assets/ico/apple-touch-icon-57-precomposed.png')); ?>">
	<link rel="shortcut icon" href="<?php echo e(asset('public/assets/ico/favicon.png')); ?>">

	<!-- CSS Plugins -->
	<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/assets/bootstrap/css/bootstrap.min.css')); ?>" media="screen">	
	<link href="<?php echo e(asset('public/assets/css/animate.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('public/assets/css/main.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('public/assets/css/component.css')); ?>" rel="stylesheet">
	
	<!-- CSS Font Icons -->
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/linearicons/style.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/font-awesome/css/font-awesome.min.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/simple-line-icons/css/simple-line-icons.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/ionicons/css/ionicons.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/rivolicons/style.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/flaticon-line-icon-set/flaticon-line-icon-set.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/flaticon-streamline-outline/flaticon-streamline-outline.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/flaticon-thick-icons/flaticon-thick.css')); ?>">
	<link rel="stylesheet" href="<?php echo e(asset('public/assets/icons/flaticon-ventures/flaticon-ventures.css')); ?>">

	<!-- CSS Custom -->
	<link href="<?php echo e(asset('public/assets/css/style.css')); ?>" rel="stylesheet">


	<link href="<?php echo e(asset('public/assets/js/plugins/magnific-popup/magnific-popup.css')); ?>" rel="stylesheet">
	<link href="<?php echo e(asset('public/assets/js/plugins/datepicker/datepicker.css')); ?>" rel="stylesheet">



	<!-- CSS Custom -->
	

	<!-- order online css -->
	<link href="<?php echo e(asset('public/assets/css/order-online.css')); ?>" rel="stylesheet" type="text/css">
</head>