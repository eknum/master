
<!-- start Back To Top -->
<div id="back-to-top">
   <a href="#"><i class="ion-ios-arrow-up"></i></a>
</div>
<!-- end Back To Top -->


<!-- JS -->
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery-1.11.3.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery-migrate-1.2.1.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/bootstrap-modalmanager.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/bootstrap-modal.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/smoothscroll.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery.easing.1.3.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery.waypoints.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/wow.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery.slicknav.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery.placeholder.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/bootstrap-tokenfield.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/typeahead.bundle.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/bootstrap-select.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery-filestyle.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/bootstrap-select.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/ion.rangeSlider.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/handlebars.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery.countimator.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery.countimator.wheel.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/slick.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/easy-ticker.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery.introLoader.min.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/jquery.responsivegrid.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/assets/js/customs.js')); ?>"></script>