<!doctype html>
<html lang="en">
<?php echo $__env->make('client.template.client_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class="home">

	<div id="introLoader" class="introLoading"></div>
	<div class="container-wrapper">
		<?php echo $__env->make('client.template.header', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

		<!-- start Main Wrapper -->
		<div class="main-wrapper">
		<?php echo $__env->yieldContent('wrapper'); ?>

		<?php echo $__env->make('client.template.footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		</div>
		<!-- end Main Wrapper -->
	</div>
	<!-- end Container Wrapper -->

	
		<?php echo $__env->make('client.template.client_js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</body>
</html>