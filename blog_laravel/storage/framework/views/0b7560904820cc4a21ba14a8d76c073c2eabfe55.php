<?php $__env->startSection('content'); ?>
 
  <br>
  <div class="container-fluid">
  <div class="row">
  <div class="col-xl-3 col-sm-6 mb-4">
                        <div class="card bg-primary border-0 text-light pt-3 pb-3 h-100">
                            <div class="card-body ">
                                <div class="row">
                                    <div class=" col-3">
                                        <i class="icon-people f30"></i>
                                    </div>
                                    <div class=" col-9">
                                        <h6 class="m-0 text-light">Total users</h6>
                                        <p class="f12 mb-0"><?php echo e($dashboard['users']); ?>Total users</p>
                                    </div>
                                </div>
                            </div>
                        </div>
   </div>
                    <div class="col-xl-3 col-sm-6 mb-4">
                        <div class="card bg-danger border-0 text-light pt-3 pb-3 h-100">
                            <div class="card-body ">
                                <div class="row">
                                    <div class=" col-3">
                                        <i class="icon-people f30"></i>
                                    </div>
                                    <div class=" col-9">
                                        <h6 class="m-0 text-light">Total restaurant</h6>
                                        <p class="f12 mb-0"><?php echo e($dashboard['restaurants']); ?>Total restaurant</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
   
              

<?php $__env->stopSection(); ?>
<?php $__env->startSection('custom_js'); ?>
<script>
    $(document).ready(function() {
        $('#bs4-table').DataTable();
    } );
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>