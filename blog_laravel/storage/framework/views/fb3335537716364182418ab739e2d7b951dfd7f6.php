 <!DOCTYPE html>
<html lang="en">

<?php echo $__env->make('admin.template.admin_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class="app header-fixed left-sidebar-light left-sidebar-light-alt left-sidebar-fixed right-sidebar-fixed right-sidebar-overlay right-sidebar-hidden">
        <?php echo $__env->make('admin.template.admin_navbar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--===========app body start===========-->
    <div class="app-body">
      <?php echo $__env->make('admin.template.admin_sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--main contents start-->
        <main class="main-content">


<div class="page-title">
      <h4 class="mb-0"> Menu  
      </h4>
</div>
<div class="container-fluid">
  <!--page title end-->
  <div class="row">
      <div class=" col-sm-12">
            
            <div class="card card-shadow mb-4">
                            <div class="card-header">
                                <div class="card-title">
                                    Justified
                                </div>
                            </div>
                            <div class="card-body">
                                <ul class="nav nav-tabs nav-fill mb-4" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="<?php echo e(route('menus.index')); ?>"> <i class="fa fa-home pr-2"></i> Menu</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="<?php echo e(route('items.index')); ?>"> Item</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab-j_3">  Tab 3</a>
                                    </li>

                                </ul>


                                <div class="tab-content">
                                    <?php echo $__env->yieldContent('tab-content'); ?>
                                    <!-- <div class="tab-pane active" id="tab-j_1" role="tabpanel">
                                        Build responsive, mobile-first projects on the web with the world's most popular front-end component library.
                                        Bootstrap is an open source toolkit for developing with HTML, CSS, and JS. Quickly prototype your ideas or build your entire app with our Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful plugins built on jQuery.
                                    </div>
                                    <div class="tab-pane" id="tab-j_2" role="tabpanel">
                                        Include Bootstrap's source Sass and JavaScript files via npm, Composer or Meteor. Package managed installs don't include documentation, but do include our build system and readme.
                                    </div>
                                    <div class="tab-pane" id="tab-j_3" role="tabpanel">
                                        Take Bootstrap 4 to the next level with official premium themes—toolkits built on Bootstrap with new components and plugins, docs, and build tools.
                                    </div> -->

                                </div>
                            </div>
                        </div>


      </div>
  </div>

</div>
</main>
 
    </div>
     <?php echo $__env->make('admin.template.admin_footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
       <?php echo $__env->make('admin.template.admin_js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
       <?php echo $__env->yieldContent('custom_js'); ?>
</body>

</html>


