
<!DOCTYPE html>

<html lang="en">
<?php echo $__env->make('admin.template.admin_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class="login_page" >

 <!--===========login start===========-->

    <div class="container">

        <form method="POST" class="form-signin" action="<?php echo e(route('login')); ?>" aria-label="<?php echo e(__('Login')); ?>">
                        <?php echo csrf_field(); ?>
            <a href="index.html" class="brand text-center">
                <h1>Login</h1>
                <!-- <img src="<?php echo e(asset('public/assets3/img/logo-dark.png')); ?>" srcset="assets/img/logo-dark@2x.png 2x" alt=""/> -->
            </a>
            <hr>
            <h2 class="form-signin-heading">Please sign in</h2>
            <div class="form-group">
                 <label for="email" class="sr-only"><?php echo e(__('E-Mail Address')); ?></label>
                 <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" placeholder="email" value="<?php echo e(old('email')); ?>" required autofocus autocomplete="off">
                  <?php if($errors->has('email')): ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('email')); ?></strong>
                    </span>
                <?php endif; ?>
                
            </div>

            <div class="form-group">
                <label for="inputPassword" class="sr-only"><?php echo e(__('Password')); ?></label>
                <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" placeholder="password" required>

                <?php if($errors->has('password')): ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('password')); ?></strong>
                    </span>
                <?php endif; ?>
                
            </div>

            <div class="checkbox mb-4 mt-4">
                <div class="form-group form-check float-left">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" <?php echo e(old('remember') ? 'checked' : ''); ?>>
                    
                    <label class="form-check-label" for="exampleCheck1"><?php echo e(__('Remember Me')); ?></label>
                </div>

                <a href="<?php echo e(route('password.request')); ?>"  class="float-right text-muted">Forgot Password ?</a>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo e(__('Login')); ?></button>

            <div class="mt-4">
                <span>
                    Don't have an account yet ?
                </span>
                <a href="<?php echo e(route('register')); ?>" class="text-primary">Sign Up</a>
            </div>
        </form>

    </div>
    <!--===========login end===========-->
    <?php echo $__env->make('admin.template.admin_js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>