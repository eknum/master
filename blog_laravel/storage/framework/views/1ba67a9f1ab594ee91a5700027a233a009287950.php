 

<?php $__env->startSection('wrapper'); ?>
		<body class="home">
		<!-- start hero-header -->
		
			<div class="hero" style="background-image:url('<?php echo e(asset('public/assets/images/hero-header/index-banner.png')); ?>');">
				<div class="container">

					<h1>Food Cloud</h1>
					<p>Find amazing selection of local restaurant delivering food to your door</p>

					<div class="main-search-form-wrapper">
					
						<form action="<?php echo e(route('restaurant.index')); ?>">
					
							<div class="form-holder">
								<div class="row gap-0">
								
									<div class="col-xss-6 col-xs-6 col-sm-6">
										<!-- <input class="form-control" placeholder="e.g. SW6 6LG / SW6 / London" /> -->
										<select name="city" class="form-control"> <label for="selection" class="sr-only"></label>
											    <option value="-1">....SELECT....</option>
                                        <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                       
                                                <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
									</div>
									
									<div class="col-xss-6 col-xs-6 col-sm-6">
										<input class="form-control" name="searchrestaurent" placeholder="Find a restaurant" />
									</div>
									
								</div>
							
							</div>
							
							<div class="btn-holder">
								<button class="btn"><i class="ion-android-search"></i></button>
							</div>
						
						</form>
						
					</div>
				

				</div>
				
			</div>
			<!-- end hero-header -->

	<div class="post-hero">
			
				<div class="container">
					
					<div class="row">
					
						<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
							
							<div class="ticker-wrapper">
				
								<div class="latest-job-ticker ticker-inner">
									
									
									<ul class="ticker-list">
										<li>
											<a href="#">
												<span class="labeling">Get Offer</span>
												Free Bottle Of Beer - <span class="font-italic">Only available through our online ordering system - Bayleaf Restauant</span>
											</a>
										</li>
										<li>
											<a href="#">
												<span class="labeling">Get Offer</span>
												10% Discount - <span class="font-italic">Breast of chicken tikka grilled. - Rainbow Restauant</span>
											</a>
										</li>

									</ul>
								</div>
							</div>
							
							
						</div>
					
					</div>
				
				</div>
			
			</div>

			<!-- start order process -->
			<div class="order-process-step bg-light pt-80 pb-80">
				<div class="container frontend">

					<div class="clearfix"></div>

					<!--Facts-->

					<!--Process Block-->

					<div class="block process-block">
						<!--<h2>The process</h2>
                            <h5>4 Steps for Success</h5>-->

						<div class="row text-center">
							<ol class="process">
								<li class="col-md-3 col-sm-6 col-xs-6"><i class="fa fa-cutlery" aria-hidden="true"></i>
									<h4>Pick a restaurant</h4>

								</li>
								<li class="col-md-3 col-sm-6 col-xs-6"> <i class="fa fa-car"></i>
									<h4>Order a takeaway</h4>

								</li>
								<li class="col-md-3 col-sm-6 col-xs-6"> <i class="fa fa-beer"></i>
									<h4>Your food is delivered</h4>

								</li>
								<li class="col-md-3 col-sm-6 col-xs-6"> <i class="fa fa-thumbs-o-up"></i>
									<h4>Happy, enjoy</h4>

								</li>
							</ol>
						</div>

						<div class="clearfix"></div>
					</div>
					<!--/Process Block-->
					<div class="clearfix"></div>

					<!--/Facts-->

				</div>
			</div>
			<!-- end order process -->
			
			<div class="pt-80 pb-80">

				<div class="container">
				
				<div class="row">
					
					<div class="col-md-8">
					
						<div class="section-title">
						
							<h2 class="text-left text-center-sm">Featured Restaurant</h2>
							
						</div>
						
						<div class="restaurant-common-wrapper">
						
							<a href="#" class="restaurant-common-wrapper-item highlight clearfix">
								<div class="GridLex-grid-middle">
									<div class="GridLex-col-6_xs-12">
										<div class="restaurant-type">
											<div class="image">
												<img src="<?php echo e(asset('public/assets/images/brands/01.jpg')); ?>" alt="image" />
											</div>
											<div class="content">
												<h4>Wow Burger</h4>
												<p>Indian</p>
											</div>
										</div>
									</div>
									<div class="GridLex-col-4_xs-8_xss-12 mt-10-xss">
										<div class="job-location">
											<i class="fa fa-map-marker text-primary"></i> Menlo park, CA
										</div>
									</div>
									<div class="GridLex-col-2_xs-4_xss-12">
										<div class="res-btn label label-danger">
											Place Order
										</div>
										<span class="font12 block spacing1 font400 text-center">Min: £15</span>
									</div>
								</div>
							</a>
							
							<a href="#" class="restaurant-common-wrapper-item clearfix">
								<div class="GridLex-grid-middle">
									<div class="GridLex-col-6_xs-12">
										<div class="restaurant-type">
											<div class="image">
												<img src="<?php echo e(asset('public/assets/images/brands/02.jpg')); ?>" alt="image" />
											</div>
											<div class="content">
												<h4>Food Republic</h4>
												<p>Indian</p>
											</div>
										</div>
									</div>
									<div class="GridLex-col-4_xs-8_xss-12 mt-10-xss">
										<div class="job-location">
											<i class="fa fa-map-marker text-primary"></i> Menlo park, CA
										</div>
									</div>
									<div class="GridLex-col-2_xs-4_xss-12">
										<div class="res-btn label label-danger">
											Place Order
										</div>
										<span class="font12 block spacing1 font400 text-center">Min: £15</span>
									</div>
								</div>
							</a>
							
							<a href="#" class="restaurant-common-wrapper-item clearfix">
								<div class="GridLex-grid-middle">
									<div class="GridLex-col-6_xs-12">
										<div class="restaurant-type">
											<div class="image">
												<img src="<?php echo e(asset('public/assets/images/brands/03.jpg')); ?>" alt="image" />
											</div>
											<div class="content">
												<h4>Attin Cafe &amp; Lounge</h4>
												<p>Indian</p>
											</div>
										</div>
									</div>
									<div class="GridLex-col-4_xs-8_xss-12 mt-10-xss">
										<div class="job-location">
											<i class="fa fa-map-marker text-primary"></i> Menlo park, CA
										</div>
									</div>
									<div class="GridLex-col-2_xs-4_xss-12">
										<div class="res-btn label label-danger">
											Place Order
										</div>
										<span class="font12 block spacing1 font400 text-center">Min: £15</span>
									</div>
								</div>
							</a>
							
							<a href="#" class="restaurant-common-wrapper-item clearfix">
								<div class="GridLex-grid-middle">
									<div class="GridLex-col-6_xs-12">
										<div class="restaurant-type">
											<div class="image">
												<img src="<?php echo e(asset('public/assets/images/brands/04.jpg')); ?>" alt="image" />
											</div>
											<div class="content">
												<h4>Khayalee Polao</h4>
												<p>Bangladeshi</p>
											</div>
										</div>
									</div>
									<div class="GridLex-col-4_xs-8_xss-12 mt-10-xss">
										<div class="job-location">
											<i class="fa fa-map-marker text-primary"></i> Menlo park, CA
										</div>
									</div>
									<div class="GridLex-col-2_xs-4_xss-12">
										<div class="res-btn label label-danger">
											Place Order
										</div>
										<span class="font12 block spacing1 font400 text-center">Min: £15</span>
									</div>
								</div>
							</a>
							<a href="#" class="restaurant-common-wrapper-item clearfix">
								<div class="GridLex-grid-middle">
									<div class="GridLex-col-6_xs-12">
										<div class="restaurant-type">
											<div class="image">
												<img src="<?php echo e(asset('public/assets/images/brands/05.jpg')); ?>" alt="image" />
											</div>
											<div class="content">
												<h4>Italian Pizza Hut</h4>
												<p>Indian</p>
											</div>
										</div>
									</div>
									<div class="GridLex-col-4_xs-8_xss-12 mt-10-xss">
										<div class="job-location">
											<i class="fa fa-map-marker text-primary"></i> Menlo park, CA
										</div>
									</div>
									<div class="GridLex-col-2_xs-4_xss-12">
										<div class="res-btn label label-danger">
											Place Order
										</div>
										<span class="font12 block spacing1 font400 text-center">Min: £15</span>
									</div>
								</div>
							</a>
						
						</div>
					
					</div>
				
					<div class="col-md-4 mt-50-sm">
					
						<div class="section-title">
						
							<h2 class="text-left text-center-sm">top seller</h2>
							
						</div>
						
						<div class="row gap-20 top-company-wrapper mmt">
						
							<div class="col-xs-6 col-sm-4 col-md-6">
								
								<div class="top-company">
									<div class="image">
										<img src="<?php echo e(asset('public/assets/images/brands/01.png')); ?>" alt="image" />
									</div>
									<h5>Vantage</h5>
									<a href="#">place order</a>
								</div>
								
							</div>
							
							<div class="col-xs-6 col-sm-4 col-md-6">
								
								<div class="top-company">
									<div class="image">
										<img src="<?php echo e(asset('public/assets/images/brands/05.png')); ?>" alt="image" />
									</div>
									<h5>Greenchilli</h5>
									<a href="#">place order</a>
								</div>
								
							</div>
							
							<div class="col-xs-6 col-sm-4 col-md-6">
							
								<div class="top-company">
									<div class="image">
										<img src="<?php echo e(asset('public/assets/images/brands/18.png')); ?>" alt="image" />
									</div>
									<h5>Bengal Spice</h5>
									<a href="#">place order</a>
								</div>
								
							</div>
							
							<div class="col-xs-6 col-sm-4 col-md-6">
								
								<div class="top-company">
									<div class="image">
										<img src="<?php echo e(asset('public/assets/images/brands/12.png')); ?>" alt="image" />
									</div>
									<h5>Magna Tandoori</h5>
									<a href="#">place order</a>
								</div>
								
							</div>
							
							<div class="col-xs-6 col-sm-4 col-md-6">
							
								<div class="top-company">
									<div class="image">
										<img src="<?php echo e(asset('public/assets/images/brands/10.png')); ?>" alt="image" />
									</div>
									<h5>New Bengla</h5>
									<a href="#">place order</a>
								</div>
								
							</div>
							
							<div class="col-xs-6 col-sm-4 col-md-6">
								
								<div class="top-company">
									<div class="image">
										<img src="<?php echo e(asset('public/assets/images/brands/08.png')); ?>" alt="image" />
									</div>
									<h5>Spice Delight</h5>
									<a href="#">place order</a>
								</div>
								
							</div>
						
						</div>
						
					</div>
					
				</div>

				</div>
				
			</div>

			<!-- start banner section -->
			<div class="bt-block-home-parallax pt-80 pb-80" style="background-image: url(<?php echo e(asset('public/assets/images/reparallax-one.jpg')); ?>);">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="lookbook-product">
								<h2>Delicious food is just a click away! Explore our enlisted restaurants – we believe, you will love them. </h2>

							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end banner section -->
			
			<div class="container pt-80 pb-80">

				<div class="row">
					
					<div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
					
						<div class="section-title">
						
							<h3>discover new restaurant &amp; book now </h3>
							
						</div>
					
					</div>
				
				</div>
				
				<div class="row gap-40">
				
					<div class="col-xs-4 col-sm-2 mb-20">
						<a href="#"><img src="<?php echo e(asset('public/assets/images/brands/10.png')); ?>" alt="image" /></a>
					</div>
					
					<div class="col-xs-4 col-sm-2 mb-20">
						<a href="#"><img src="<?php echo e(asset('public/assets/images/brands/02.png')); ?>" alt="image" /></a>
					</div>
					
					<div class="col-xs-4 col-sm-2 mb-20">
						<a href="#"><img src="<?php echo e(asset('public/assets/images/brands/04.png')); ?>" alt="image" /></a>
					</div>
					
					<div class="col-xs-4 col-sm-2 mb-20">
						<a href="#"><img src="<?php echo e(asset('public/assets/images/brands/18.png')); ?>" alt="image" /></a>
					</div>
					
					<div class="col-xs-4 col-sm-2 mb-20">
						<a href="#"><img src="<?php echo e(asset('public/assets/images/brands/16.png')); ?>" alt="image" /></a>
					</div>
					
					<div class="col-xs-4 col-sm-2 mb-20">
						<a href="#"><img src="<?php echo e(asset('public/assets/images/brands/14.png')); ?>" alt="image" /></a>
					</div>
				
				</div>

			
			</div>


			<!-- Download App Start -->
			<div class="download-app-area">
				<div class="download-app-sec" style="background:url(<?php echo e(asset('public/assets/images/download-app.jpg')); ?>) bottom center no-repeat fixed;
        background-size:cover;">
					<div class="mask">
						<div class="container">
							<div class="col-lg-7 col-md-7 col-sm-12 container-cell left-container col-md-push-1">
								<div class="app-content row">
									<div class="inner">
										<h2 class="logo-content">Thefoody in your pocket!</h2>
										<h4 class="logo-subtitle">Get our app, it's the fastest way to order food on the go.</h4>
										<!--<p class="content">
                                            Keep an eye on Thefoody, it is already on your way. Come back here for checkout the latest updates.
                                        </p>-->
										<ul class="list-inline appstore-buttons">
											<li><a href="#" class="btn-store btn-appstore">App Store</a></li>
											<li><a href="#" class="btn-store btn-googleplay">Google Play</a></li>
										</ul>
									</div>

								</div>
							</div>
							<div class="col-md-4 right-align">
								<div class="left-area visible-lg">
									<img src="<?php echo e(asset('public/assets/images/mobilev2.png')); ?>" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<!--Download App End -->
			<!-- start Back To Top -->
		<div id="back-to-top">
   			<a href="#"><i class="ion-ios-arrow-up"></i></a>
		</div>	
		<!-- end Back To Top -->
		

<?php $__env->stopSection(); ?>

<?php echo $__env->make('client.client_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>