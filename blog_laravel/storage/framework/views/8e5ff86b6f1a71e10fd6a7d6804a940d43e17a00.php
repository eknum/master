<!-- left sidebar start-->
<div class="left-sidebar">
    <nav class="sidebar-menu">
        <ul id="nav-accordion">
            <li class="sub-menu">
                <!-- <a href="javascript:;" class="active">
                    <i class=" ti-home"></i> -->
                    <a  href="<?php echo e(route('dashboard')); ?>" class="<?php echo e(Route::current()->getName() == 'dashboard' ? 'active' : ''); ?>">
                    <span>Dashboard</span>
                    </a>          
                    <a  href="<?php echo e(route('users.index')); ?>" class="<?php echo e(Route::current()->getName() == 'users.index' ? 'active' : ''); ?>">
                    <span>Users</span>                 
                    </a>
                    <a href="<?php echo e(route('restaurants.index')); ?>" class="<?php echo e(Route::current()->getName() == 'restaurants.index' ? 'active' : ''); ?>">
                    <span>Restaurant</span> 
                    </a> 
                    <a href="<?php echo e(route('menus.index')); ?>" class="<?php echo e(Route::current()->getName() == 'menus.index' ? 'active' : ''); ?>">
                    <span>Menu</span> 
                    </a>  
                    <a href="<?php echo e(route('setting.index')); ?>" class="<?php echo e(Route::current()->getName() == 'setting.index' ? 'active' : ''); ?>">
                    <span>General Setting</span> 
                    </a>              
                           
                
            </li>    
        </ul>
    </nav>
</div>
<!--left sidebar end