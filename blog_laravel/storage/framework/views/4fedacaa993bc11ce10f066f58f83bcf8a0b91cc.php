 

<?php $__env->startSection('content'); ?>

        <!--main contents start-->
        <main class="main-content">
            <!--page title start-->
            <div class="page-title">
                <h4 class="mb-0"> Edit Data
                </h4>
                <ol class="breadcrumb mb-0 pl-0 pt-1 pb-0">
                    <!-- <li class="breadcrumb-item"><a href="#" class="default-color">Home</a></li>
                    <li class="breadcrumb-item active">Basic Input</li> -->
                </ol>
            </div>
            <!--page title end-->
            <div class="container-fluid">

                <!-- state start-->
                <div class="row">
                    <div class=" col-md-6">           
                        <div class="card card-shadow mb-4">
                           
                            <div class="card-body">
                                <form method="post" action="<?php echo e(route('items.update')); ?>" >
                                    <?php echo e(csrf_field()); ?>

                                    
                                    <input type="hidden" name="id" value="<?php echo e($data->id); ?>">
                
                                    <div class="form-group">
                                        <label for="name"><?php echo e(__('Name')); ?></label>
                                        <input type="text" name="name" class="form-control form-control-pill" id="name" value="<?php echo e($data->name); ?>">
                                    </div>
                                     <div class="form-group">
                                        <label for="name"><?php echo e(__('Price')); ?> <span style="color: red">*</span></label>
                                        <input type="number" name="price" class="form-control form-control-pill<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" id="price" placeholder="item price" value="<?php echo e($data->price); ?>" required autofocus>
                                         <?php if($errors->has('price')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('price')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="description"><?php echo e(__('Description')); ?> <span style="color: red">*</span></label>
                                        <input type="description" name="description" class="form-control form-control-pill<?php echo e($errors->has('description') ? ' is-invalid' : ''); ?>" id="description" placeholder="item name" value="<?php echo e($data->description); ?>" required autofocus>
                                         <?php if($errors->has('description')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('description')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                     <div class="form-group">Menu &nbsp;&nbsp;
                                     <select name="menu" class="form-control form-control-pill"> <label for="selection" class="sr-only"></label>
                                        <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>   
                                        <option value="<?php echo e($value->id); ?>"  <?php if($data->menu_id == $value->id ): ?> <?php echo "selected"; ?> <?php endif; ?>><?php echo e($value->name); ?></option>                                    
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                       
                                    </div>

                                    <button class="btn btn-lg btn-primary btn-block" name="edit" type="submit"><?php echo e(__('Edit')); ?></button>
                                </form>

                            </div>
                        </div>

                    </div>
                   
                </div>

                <!-- state end-->

            </div>
        </main>
        <!--main contents end-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('custom_js'); ?>
<script>
    $(document).ready(function() {
        $('#bs4-table').DataTable();
    } );
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>