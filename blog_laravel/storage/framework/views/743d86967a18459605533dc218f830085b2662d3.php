 

<?php $__env->startSection('wrapper'); ?>
<div class="second-search-restaurant-wrapper">
			
				<div class="container">
				
					<form action="<?php echo e(route('restaurant.index')); ?>">
					
						<div class="second-search-result-inner">
							<span class="labeling">Search a restaurant</span>
							<div class="row">
							
								<div class="col-xss-12 col-xs-6 col-sm-6 col-md-5">
									<div class="form-group form-lg">
										<!-- <input type="text" class="form-control" placeholder="e.g. SW6 6LG / SW6 / London" /> -->
                                     <select name="city" class="form-control form-control-pill"> <label for="selection" class="sr-only"></label>
                                     	<option value='-1'>....SELECT....</option>
                                        <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                       
                                        <?php if($cityID == $value->id): ?>
                                                <option value="<?php echo e($value->id); ?>" selected><?php echo e($value->name); ?></option>
                                          <?php else: ?>
                                      			<option value="<?php echo e($value->id); ?>" ><?php echo e($value->name); ?></option>
                                      	<?php endif; ?>
                                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                       
									</div>
								</div>
								
								<div class="col-xss-12 col-xs-6 col-sm-6 col-md-5">
									<div class="form-group form-lg">
										<input type="text" name="searchrestaurent" class="form-control" placeholder="Find a restaurant" value="<?php echo e($resutantName); ?>" />
									</div>
								</div>
								
								<div class="col-xss-12 col-xs-6 col-sm-4 col-md-2">
									<button class="btn btn-block">Search</button>
								</div>
							
							</div>
						</div>
					
					</form>

				</div>
			
			</div>
			<body class="not-transparent-header">
				<!-- start hero-header -->

			<div class="breadcrumb-wrapper">
			
				<div class="container">
				
					<ol class="breadcrumb-list booking-step">
						<li><a href="<?php echo e(route('client.index')); ?>">Home</a></li>
						<li><span>All restaurant</span></li>
					</ol>
					
				</div>
				
			</div>
			<!-- end hero-header -->
			<div class="section sm">
			
				<div class="container">
				
					<div class="sorting-wrappper">
			
						<div class="sorting-header">
							<h3 class="sorting-title">We found <?php echo e(count($restaurant)); ?> restaurant</h3>
						</div>
						
						<div class="sorting-content">

							<div class="row">
							
								<div class="col-sm-12 col-md-7">
								
									<div class="row gap-10">
									
										<div class="col-sm-4">
											<div class="form-group">
												<label>Service:</label>
												<select class="selectpicker form-control" data-show-subtext="true">
													<option value="0">Reservation</option>
													<option value="1" >Delivery</option>
													<option value="2" >Collection</option>
												</select>
											</div>
										</div>
										
										<div class="col-sm-4">
											<div class="form-group">
												<label>Popular City</label>
												<select class="selectpicker form-control" data-show-subtext="true">
													<option value="0">Aberdeen</option>
													<option value="1">Birmingham</option>
													<option value="2">Blackburn</option>
													<option value="3">Bristol</option>
													<option value="4">Croydon</option>
													<option value="5">Dudley</option>
												</select>
											</div>
										</div>
										
										<div class="col-sm-4">
											<div class="form-group">
												<label>Cuisine</label>
												<select class="selectpicker show-tick form-control" data-selected-text-format="count > 3" data-done-button="true" data-done-button-text="OK" multiple>
													<option value="0" selected>All</option>
													<option value="1">Burger</option>
													<option value="2">Chicken</option>
													<option value="3">Chinese</option>
													<option value="4">English</option>
													<option value="6">Fast Food</option>
													<option value="7">Grill</option>
													<option value="7">Indian</option>
													<option value="7">Italian</option>
													<option value="7">Thai</option>
												</select>
											</div>
										</div>
										
									</div>

								</div>
								
								<div class="col-sm-12 col-md-5">
									
									<div class="text-right hidden-sm hidden-xs">
										<button class="btn btn-toggle btn-refine collapsed" data-toggle="collapse" data-target="#refine-result">More filters</button>
									</div>
									
									<div class="job-type-checkbox-wrapper clearfix">

										<div class="checkbox-block job-type-checkbox freelance-checkbox">
											<input id="job_type_checkbox-3" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" checked />
											<label class="" for="job_type_checkbox-3">Reservation</label>
										</div>
										
										<div class="checkbox-block job-type-checkbox part-time-checkbox">
											<input id="job_type_checkbox-2" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" checked />
											<label class="" for="job_type_checkbox-2">Delivery</label>
										</div>
										
										<div class="checkbox-block job-type-checkbox full-time-checkbox">
											<input id="job_type_checkbox-1" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" checked />
											<label class="" for="job_type_checkbox-1">Collection</label>
										</div>
										
									</div>

									
									<div class="clear"></div>
									
									<div class="text-right visible-sm visible-xs">
										<button class="btn btn-toggle btn-refine collapsed" data-toggle="collapse" data-target="#refine-result">More filters</button>
									</div>

								</div>
								
							</div>

							<div id="refine-result" class="collapse">

								<div class="collapse-inner clearfix">
								
									<div class="row">
									
										<div class="col-sm-12 col-md-7">
									
											<div class="job-type-checkbox-wrapper visible-sm visible-xs clearfix">
															
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-4-mobile" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-4-mobile">Open Now</label>
												</div>
												
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-5-mobile" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-5-mobile">Distance</label>
												</div>
												
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-6-mobile" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-6-mobile">User Rating</label>
												</div>
												
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-7-mobile" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-7-mobile">Restaurant Name</label>
												</div>
												
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-8-mobile" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-8-mobile">Permanent</label>
												</div>
												
											</div>

											<div class="row gap-10">



												<div class="col-sm-12">
													<div class="form-group">
														<label>Rating:</label>
														<div class="sidebar-module-inner">
															<input id="price_range" />
														</div>
													</div>
												</div>

											</div>
											


										</div>
										
										<div class="col-sm-12 col-md-5 hidden-sm hidden-xs">
											
											<div class="job-type-checkbox-wrapper clearfix">
													
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-4" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-4">Distance</label>
												</div>
												
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-5" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-5">Recommended</label>
												</div>
												
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-6" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-6">Price</label>
												</div>
												
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-7" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-7">Rating</label>
												</div>
												
												<div class="checkbox-block job-type-checkbox">
													<input id="job_type_checkbox-8" name="job_type_checkbox" type="checkbox" class="checkbox" value="First Choice" />
													<label class="" for="job_type_checkbox-8">Open Now</label>
												</div>
												
											</div>

										</div>
										
									</div>
									
								</div>
							
							</div>
			
						</div>
		
					</div>
					
					
                      			

 			<div class="restaurant-wrapper">
					
						<div class="row">
						
							<div class="col-sm-8 col-md-9 mt-25">
							
								<div class="restaurant-list-wrapper">
									 
                      				<?php $__currentLoopData = $restaurant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

									
									<div class="restaurant-item-list">
									
										<div class="image">
											<!-- <img src="images/brands/02.png" alt="image" /> -->
											<img src="<?php echo e(asset("storage/app/$value->image")); ?>" width="100" height="100">
										</div>
										
										<div class="content">
											<div class="restaurant-item-list-info">
											
												<div class="row">
												
													<div class="col-sm-7 col-md-8">
													
														<h4 class="heading"><?php echo e($value->id); ?><?php echo e($value->name); ?></h4>
														<div class="meta-div clearfix mb-25">
															<span>at <a href="#">Bangladeshi</a></span>
															<span class="res-btn label label-success">Get Offer</span>
														</div>
														<?php echo e($value->description); ?>

														
													</div>

													<div class="col-sm-5 col-md-4">
														<ul class="meta-list">
															<li>
																<span>Location:</span>
                         										<?php echo e($value->city ? $value->city->name : ''); ?>,

																<?php echo e($value->address); ?>


															</li>
															<li>
																<span>Min Order:</span>
																<?php echo e($value->minorder); ?>

																
															</li>
															<li>
																<span>Open time:</span>
																<?php echo e($value->open_time); ?>

																
															</li>
															<li>
																<span>Close time: </span>
																<?php echo e($value->close_time); ?>

															</li>
														</ul>
													</div>
													
												</div>
											
											</div>
										
											<div class="restaurant-item-list-bottom">
											
												<div class="row">
												
													<div class="col-sm-7 col-md-8">
														<div class="sub-category">
															<span>
																	<span style="color: #ED1C3B;"><i class="fa fa-star"></i></span>
																		<span style="color: #ED1C3B;"><i class="fa fa-star"></i></span>
																		<span style="color: #ED1C3B;"><i class="fa fa-star"></i></span>
																		<span style="color: #CCCCCC;"><i class="fa fa-star"></i></span>
																		<span style="color: #CCCCCC;"><i class="fa fa-star"></i></span>
																		<span class="review">
																					(<span>15</span>)
																		</span>
															</span>
														</div>
													</div>
													
													<div class="col-sm-5 col-md-4">
														<a href="<?php echo e(route('restaurant.order.index', $value->id)); ?>" class="btn btn-primary">Place order</a>
													</div>
													
												</div>
											
											</div>
											<!-- start map -->
											<div class="restaurant-item-list-bottom">
											
												<div class="row">
												
													<div class="col-sm-7 col-md-8">

													</div>
												</div>
											</div>
											<!-- end map -->
										</div>
									
									</div>

									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				
								</div>  
								<div class="pager-wrapper">
								
									<ul class="pager-list">
										<li class="paging-nav"><a href="#"><i class="fa fa-angle-double-left"></i></a></li>
										<!-- <li class="paging-nav"><a href="#"><i class="fa fa-angle-left"></i></a></li> -->
										<li class="number">
											<span class="mr-5"><span class="font600">page</span></span>
										</li>
										<li class="form">
											<form>
												<input type="text" value="1" class="form-control"> 
											</form>
										</li>
										<li class="number">
											<span class="mr-5">/</span> <span class="font600">79</span>
										</li>
										<li class="paging-nav"><a href="#">go</a></li>
										<!-- <li class="paging-nav"><a href="#"><i class="fa fa-angle-right"></i></a></li> -->
										<li class="paging-nav"><a href="#"><i class="fa fa-angle-double-right"></i></a></li>
									</ul>	
								
								</div>
								
							</div>
							
							<!-- <div class="col-sm-4 col-md-3 mt-25">
							
								<div class="restaurant-list-wrapper">
								
									<aside class="sidebar with-filter">
									
										<div class="sidebar-inner">
										
											<div class="sidebar-module">
												<h4 class="sidebar-title">Featured Restaurant</h4>
						
												<div class="top-company-2-wrapper">
												
													<div class="GridLex-gap-10">
			
														<div class="GridLex-grid-noGutter-equalHeight">
											
															<div class="GridLex-col-12_sm-12_xs-6_xss-12">
															
																<div class="top-company-2">
																	<a href="#">
																	
																		<div class="image">
																			<img src="images/brands/08.png" alt="image" />
																		</div>
																		
																		<div class="content">
																			<h5 class="heading text-primary font700">Spice Delight</h5>
																			<p class="texting font600">Lorem ipsum dolor sit amet, consectetur and much more...</p>
																			<p class="mata-p clearfix"><span class="text-primary font700">&pound;15</span> <span class="font13">min order</span> <span class="pull-right icon"><i class="fa fa-long-arrow-right"></i></span></p>
																		</div>
																	
																	</a>
																	
																</div>
																
															</div>
															
															<div class="GridLex-col-12_sm-12_xs-6_xss-12">
															
																<div class="top-company-2">
																	<a href="#">
																	
																		<div class="image">
																			<img src="images/brands/09.png" alt="image" />
																		</div>
																		
																		<div class="content">
																			<h5 class="heading text-primary font700">Fish Chips</h5>
																			<p class="texting font600">Lorem ipsum dolor sit amet, consectetur and much more...</p>
																			<p class="mata-p clearfix"><span class="text-primary font700">&pound;15</span> <span class="font13">min order</span> <span class="pull-right icon"><i class="fa fa-long-arrow-right"></i></span></p>
																		</div>
																	
																	</a>
																	
																</div>
																
															</div>
															
															<div class="GridLex-col-12_sm-12_xs-6_xss-12">
																
																<div class="top-company-2">
																	<a href="#">
																	
																		<div class="image">
																			<img src="images/brands/05.png" alt="image" />
																		</div>
																		
																		<div class="content">
																			<h5 class="heading text-primary font700">Green Chilli</h5>
																			<p class="texting font600">Lorem ipsum dolor sit amet, consectetur and much more...</p>
																			<p class="mata-p clearfix"><span class="text-primary font700">&pound;15</span> <span class="font13">min order</span> <span class="pull-right icon"><i class="fa fa-long-arrow-right"></i></span></p>
																		</div>
																	
																	</a>
																	
																</div>
																
															</div>
															
														</div>
													
													</div>

												</div>
												
											</div>
											
											<div class="sidebar-module">
												<h4 class="sidebar-title">Top Locations</h4>
						
												<ul class="sidebar-link-list">
													<li><a href="#">Louisville, KY <span>(432)</span></a></li>
													<li><a href="#">Charleroi, PA <span>(123)</span></a></li>
													<li><a href="#">New York, NY<span>(3332)</span></a>
														<ul class="sidebar-link-list active">
															<li><a href="#">Mahattan<span>(23)</span></a></li>
															<li><a href="#">Broadway<span>(43)</span></a></li>
															<li><a href="#">Midtown<span>(09)</span></a></li>
															<li><a href="#">Brooklyn<span>(18)</span></a></li>
															<li><a href="#">Wall Streat<span>(65)</span></a></li>
														</ul>
													</li>
													<li><a href="#">Richmond, VA <span>(134)</span></a></li>
													<li><a href="#">Overland Park, KS <span>(321)</span></a></li>
													<li><a href="#">Dallas, TX <span>(554)</span></a></li>
													<li><a href="#">Irvine, CA <span>(66)</span></a></li>
													<li><a href="#">Escondido, CA <span>(78)</span></a></li>
												</ul>
												
											</div>
											
											<div class="sidebar-module">
												<h4 class="sidebar-title">New Restaurant</h4>
						
												<div class="sub-category">
													<a href="#">Bangkok Lounge</a>
													<a href="#">Arabian Grill</a>
													<a href="#">Curry Garden</a>
													<a href="#">Green Chilli</a>
													<a href="#">Bangkok Takeaway</a>
													<a href="#">Curry Garden</a>
													<a href="#">Bangkok Lounge</a>
													<a href="#">Arabian Grill</a>
													<a href="#">Curry Garden</a>
													<a href="#">Green Chilli</a>
												</div>
												
											</div>
											
										</div>
										
									</aside>
									
								</div>
								
							</div> -->
						
						</div>
						
					</div>

				</div>
			
			</div>

			
			
		</div>
		

	</div>


<?php $__env->stopSection(); ?>
		


<?php echo $__env->make('client.client_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>