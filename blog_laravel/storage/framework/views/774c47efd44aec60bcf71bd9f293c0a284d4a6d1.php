 

<?php $__env->startSection('content'); ?>

<div class="page-title">
      <h4 class="mb-0"> Restaurant
          
      </h4>
  </div>
<div class="container-fluid">
  <!--page title end-->
  <div class="row">
      <div class=" col-sm-12">
          <div class="card card-shadow mb-4">
              <div class="card-header">
                  <div class="card-title">
                     
                  </div>
                 <!-- <button style="float:right"><a href="<?php echo e(route('restaurants.create')); ?>"> Add Restaurant</a></button> -->
                 <a href="<?php echo e(route('restaurants.create')); ?>" class="btn btn-primary" style="float: right;"><i class="fa fa-fw fa-plus"></i> Add Restaurant</a>
              </div>

              <div class="card-body">
                  <table id="bs4-table" class="table table-bordered table-striped" cellspacing="0">

                       <thead>
                      <tr>                        
                          <th>Id</th>
                          <th>Image</th>
                          <th>Restaurant Name</th>
                          <th>Email</th>
                          <th>Address</th>
                          <th>City</th>
                          <th>Description</th>
                          <th>Min Order</th>
                          <th>Status</th>
                          <th>delivery</th>
                          <th>Opening Time</th>
                          <th>Closing Time</th>
                          
                          <th>Action</th>
                      </tr>
                      </thead>
                      <?php $__currentLoopData = $restaurant; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <tr>
                      	 <td><?php echo e($value->id); ?></td>
                         

                         <td><img src="<?php echo e(asset("storage/app/$value->image")); ?>" alt="<?php echo e($value->name); ?>" width="100" height="100"></td>
                         <td><?php echo e($value->name); ?></td>
                         <td><?php echo e($value->email); ?></td>
                         <td><?php echo e($value->address); ?></td>
                         <td><?php echo e($value->city ? $value->city->name : ''); ?></td> 

                         <td><?php echo e($value->description); ?></td>
                         
                         <td><?php echo e($value->minorder); ?></td>

                        <td><?php echo e(config('constant.status')[$value->status]); ?></td> 
                         <td><?php echo e(config('constant.delivery')[$value->delivery]); ?></td>
                         
                         <td><?php echo e($value->open_time); ?></td>
                         <td><?php echo e($value->close_time); ?></td>

                         <td><a class="btn btn-primary" href="<?php echo e(route('restaurants.edit',$value->id)); ?>"><i class="fa fa-fw fa-pencil"></i>Edit</a>&nbsp;
                              <a class="btn btn-danger" href="<?php echo e(route('restaurants.delete',$value->id)); ?>" onclick="return confirm('are you sure you want to delete??')"><i class="fa fa-fw fa-trash-o"></i>Delete</a>
                         </td>
                     </tr>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> 


                     
                  </table>
              </div>
          </div>
      </div>
  </div>

</div>



<?php $__env->stopSection(); ?>
<?php $__env->startSection('custom_js'); ?>
<script>
    $(document).ready(function() {
        $('#bs4-table').DataTable({
          "order": [[ 0, "desc" ]]
        });
    } );
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>