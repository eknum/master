<?php $__env->startSection('content'); ?>

        <!--main contents start-->

        <main class="main-content">
            <!--page title start-->
            <div class="page-title">
                <h4 class="mb-0"> Add New Restaurant
                </h4>
        
            </div>
            <!--page title end-->


            <div class="container-fluid">

                <!-- state start-->
                <div class="row">
                    <div class=" col-md-6">
                        <div class="card card-shadow mb-4">
                        
                            <div class="card-body">
                                <form method="post" action="<?php echo e(route('restaurants.store')); ?>" enctype="multipart/form-data">
                                    <?php echo e(csrf_field()); ?>

                                    <input type="hidden" name="id" >
                                    <div class="form-group">
                                        <label for="image"><?php echo e(__('Restaurant Image')); ?></label>
                                        <input type="file" name="image" class="form-control form-control-pill<?php echo e($errors->has('image') ? ' is-invalid' : ''); ?>" id="image" value="<?php echo e(old('image')); ?>" required autofocus>
                                         <?php if($errors->has('image')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('image')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="name"><?php echo e(__('Restaurant Name')); ?> <span style="color: red">*</span></label>
                                        <input type="text" name="name" class="form-control form-control-pill<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" id="name" placeholder="restaurant name" value="<?php echo e(old('name')); ?>" required autofocus>
                                         <?php if($errors->has('name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('name')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="email"><?php echo e(__('E-Mail')); ?> <span style="color: red">*</span></label>
                                        <input type="email" class="form-control form-control-pill<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" name="email" placeholder="email" value="<?php echo e(old('email')); ?>" required>
                                        <?php if($errors->has('email')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                   
                                     <div class="form-group" id="pac-container">
                                        <label for="address"><?php echo e(__('Address')); ?></label>
                                        <input id="txtPlaces" class="form-control form-control-pill<?php echo e($errors->has('address') ? ' is-invalid' : ''); ?>"  name="address" placeholder="address" value="<?php echo e(old('address')); ?>"  type="text" required>
                                        <?php if($errors->has('address')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('address')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div> 
                                    
                                    <div class="form-group">City &nbsp;&nbsp;
                                     <select name="city" class="form-control form-control-pill"> <label for="selection" class="sr-only"></label>
                                        <?php $__currentLoopData = $cities; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                       
                                                <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>

                                     <div class="form-group">
                                        <label for="description"><?php echo e(__('Description')); ?></label>
                                        <input type="text" class="form-control form-control-pill<?php echo e($errors->has('description') ? ' is-invalid' : ''); ?>" id="description" name="description" placeholder="description" value="<?php echo e(old('city')); ?>" required>
                                        <?php if($errors->has('description')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('description')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group">Menu &nbsp;&nbsp;
                                     <select name="menu[]" class="form-control form-control-pill" multiple="multiple"> <label for="selection" class="sr-only"></label>
                                        <?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                       
                                                <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>
                                    
                                    <!-- <div class="form-group">Item &nbsp;&nbsp;
                                     <select name="item[]" class="form-control form-control-pill" multiple="multiple"> <label for="selection" class="sr-only"></label>
                                        <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>                                       
                                                <option value="<?php echo e($value->id); ?>"><?php echo e($value->name); ?></option>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div> -->
                                     <div class="form-group">
                                        <label for="description"><?php echo e(__('Min Order')); ?></label>
                                        <input type="number" class="form-control form-control-pill<?php echo e($errors->has('minorder') ? ' is-invalid' : ''); ?>" id="description" name="minorder" value="<?php echo e(old('minorder')); ?>" required>
                                        <?php if($errors->has('minorder')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('minorder')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>    
                                     <div class="form-group">         

                                    <label for="delivery_type"><?php echo e(__('Status')); ?></label>
                                        <div class="col-sm-10">
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input <?php echo e($errors->has('status') ? ' is-invalid' : ''); ?>" type="radio" name="status" value="1" checked>
                                                    Active
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input <?php echo e($errors->has('status') ? ' is-invalid' : ''); ?>" type="radio" name="status" value="0" >
                                                    In Active
                                                </label>
                                            </div>
                                             <?php if($errors->has('status')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('status')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                        </div>
                                        </div>                                     
                                   <div class="form-group">
                                        <div class="form-group">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <label>
                                             <input class="form-check-input" name="delivery" type="checkbox" value="1"> Home Delivery
                                              </label>
                                    
                                        </div>    
                                    </div>

                                    <div class="form-group">
                                        <label for="address"><?php echo e(__('Open Time')); ?> <span style="color: red">*</span></label>
                                        <input type="time" class="form-control form-control-pill<?php echo e($errors->has('open_time') ? ' is-invalid' : ''); ?>" id="open_time" name="open_time" value="<?php echo e(old('open_time')); ?>" required>
                                        <?php if($errors->has('open_time')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('open_time')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>


                                     <div class="form-group">
                                        <label for="address"><?php echo e(__('Close Time')); ?>&nbsp;<span style="color: red">*</span></label>
                                        <input type="time" class="form-control form-control-pill<?php echo e($errors->has('close_time') ? ' is-invalid' : ''); ?>" id="close_time" name="close_time" value="<?php echo e(old('close_time')); ?>" required>
                                        <?php if($errors->has('close_time')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('close_time')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>

                                    
                                    <button class="btn btn-lg btn-primary btn-block" name="edit" type="submit"><?php echo e(__('Submit')); ?></button>
                                </form>

                            </div>
                        </div>

                    </div>
                   
                </div>

                <!-- state end-->

            </div>
        </main>
        <!--main contents end-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('custom_js'); ?>
<script>
    $(document).ready(function() {
        $('#bs4-table').DataTable();
    } );
</script>

    
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>