 

<?php $__env->startSection('wrapper'); ?>


			<body class="not-transparent-header">
				<!-- start hero-header -->

			<div class="breadcrumb-wrapper">
			
				<div class="container">
				
					<ol class="breadcrumb-list">
						<li><a href="<?php echo e(url('/')); ?>">Home</a></li>
						<li><span>Login</span></li>
					</ol>
					
				</div>
				
			</div>
			<!-- end hero-header -->

				<div class="error-page-wrapper">
		
				<div class="container">

					<div class="row">
							<!-- login container -->
							<div class="login-container">
								<!-- Combined Form Content -->
								<div class="login-container-content">
									<ul class="nav nav-tabs nav-justified">
										<li class="active link-one"><a href="#login-block" data-toggle="tab"><i class="fa fa-sign-in"></i>Sign In</a></li>
										<li class="link-two"><a href="#register-block" data-toggle="tab"><i class="fa fa-pencil"></i>Sign Up</a></li>
										<li class="link-three"><a href="#contact-block" data-toggle="tab"><i class="fa fa-envelope"></i>Contact</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active fade in" id="login-block">
											<!-- Login Block Form -->
											<div class="login-block-form">
												<!-- Heading -->
												<h4>Sign In to your Account</h4>
												<!-- Border -->
												<div class="bor bg-orange"></div>
												<!-- Form -->
												<form class="form" role="form" method="POST" class="form-signin" action="<?php echo e(route('login')); ?>">
													<!-- Form Group -->
													<?php echo csrf_field(); ?>
													 <div class="form-group">
										                 <label for="email"><?php echo e(__('E-Mail Address')); ?></label>
										                 <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" placeholder="email" value="<?php echo e(old('email')); ?>" required autofocus autocomplete="off">
										                  <?php if($errors->has('email')): ?>
										                    <span class="invalid-feedback" role="alert">
										                        <strong><?php echo e($errors->first('email')); ?></strong>
										                    </span>
										                <?php endif; ?>
                
           											 </div>

											            <div class="form-group">
											                <label for="inputPassword"><?php echo e(__('Password')); ?></label>
											                <input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" placeholder="password" required>

											                <?php if($errors->has('password')): ?>
											                    <span class="invalid-feedback" role="alert">
											                        <strong><?php echo e($errors->first('password')); ?></strong>
											                    </span>
											                <?php endif; ?>
											                
											            </div>



													<div class="form-group">
														<div class="checkbox">
															<label>
																<input type="checkbox"> Remember Me
															</label>
														</div>
													</div>
													<div class="form-group">
														<!-- Button -->
														<button type="submit" class="btn btn-primary">Sign In</button>&nbsp;
														<button type="submit" class="btn btn-primary btn-inverse">Reset</button>
													</div>
													<div class="form-group">
														<a href="account-forgot-password-page.html" class="black">Forget Password ?</a>
													</div>
												</form>
											</div>
										</div>
										<div class="tab-pane fade" id="register-block">
											<div class="register-block-form">
												<!-- Heading -->
												<h4>Create the New Account</h4>
												<!-- Border -->
												<div class="bor bg-orange"></div>
												<!-- Form -->
													<form method="POST" class="form-signin" action="<?php echo e(route('register')); ?>" aria-label="<?php echo e(__('register')); ?>">
														<?php echo csrf_field(); ?>
													<!-- Form Group -->
													<div class="form-group">
														<!-- Label -->
														<label class="control-label"><?php echo e(__('Name')); ?></label>
														<!-- Input -->
														<input id="name"  type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" placeholder="Enter Name" value="<?php echo e(old('name')); ?>" required autofocus>
														 <?php if($errors->has('name')): ?>
                    										<span class="invalid-feedback" role="alert">
                        										<strong><?php echo e($errors->first('name')); ?></strong>
                   											 </span>
              											  <?php endif; ?>

													</div>
													<div class="form-group">
														<label class="control-label"><?php echo e(__('E-Mail Address')); ?></label>
														<input id="email" type="text" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email"  placeholder="Enter Email" value="<?php echo e(old('email')); ?>" required>
														  <?php if($errors->has('email')): ?>
                    										<span class="invalid-feedback" role="alert">
                        										<strong><?php echo e($errors->first('email')); ?></strong>
                   											 </span>
              											  <?php endif; ?>

													</div>
													<div class="form-group">
														<label class="control-label"><?php echo e(__('Password')); ?></label>
														<input id="password" type="password" class="form-control<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" name="password" placeholder="Enter Password" required>
														 <?php if($errors->has('password')): ?>
										                    <span class="invalid-feedback" role="alert">
										                        <strong><?php echo e($errors->first('password')); ?></strong>
										                    </span>
										                <?php endif; ?>

													</div>
													<div class="form-group">
														<label class="control-label"><?php echo e(__('Confirm Password')); ?></label>
														<input id="password-confirm" type="password" class="form-control" placeholder="Re-type password again" name="password_confirmation" required>
													</div>
													<input type="hidden" name="type" value="2">
													<div class="form-group">
														<!-- Checkbox -->
														<div class="checkbox">
															<label>
																<input type="checkbox"> By register, I read & accept  <a href="#">the terms</a>
															</label>
														</div>
													</div>
													<div class="form-group">
														<!-- Buton -->
														<button type="submit" class="btn btn-primary">Submit</button>&nbsp;
														<button type="submit" class="btn btn-primary btn-inverse">Reset</button>
													</div>
												</form>
											</div>
										</div>
										<div class="tab-pane fade" id="contact-block">
											<!-- Contact Block Form -->
											<div class="contact-block-form">
												<h4>Contact Form</h4>
												<!-- Border -->
												<div class="bor bg-orange"></div>
												<!-- Form -->
												<form class="form" role="form">
													<!-- Form Group -->
													<div class="form-group">
														<label class="control-label">Name</label>
														<input type="text" class="form-control" placeholder="Enter Name">
													</div>
													<div class="form-group">
														<label class="control-label">Email</label>
														<input type="text" class="form-control" placeholder="Enter Email">
													</div>
													<div class="form-group">
														<label class="control-label">Subject</label>
														<input type="text" class="form-control" placeholder="Enter Subject">
													</div>
													<div class="form-group">
														<label for="comments" class="control-label">Comments</label>
														<textarea class="form-control" id="comments" rows="5" placeholder="Enter Comments"></textarea>
													</div>
													<div class="form-group">
														<!-- Buton -->
														<button type="submit" class="btn btn-primary">Submit</button>&nbsp;
														<button type="submit" class="btn btn-primary btn-inverse">Reset</button>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
							

					
					</div>
				
				</div>
			
			</div>


<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.client_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>