 

<?php $__env->startSection('wrapper'); ?>

<!-- start hero-header -->
			<div class="breadcrumb-wrapper">
			
				<div class="container">
				
					<ol class="breadcrumb-list">
						<li><a href="index.html">Home</a></li>
						<li><span>Forgot Password</span></li>
					</ol>
					
				</div>
				
			</div>
<!-- end hero-header -->

<div class="login-container-wrapper">	
	
				<div class="container">

					<div class="row">
					
						<div class="col-md-10 col-md-offset-1">
						
							<div class="row">

								<div class="col-sm-6 col-sm-offset-3">
								
									<div class="login-box-wrapper">
							
										<div class="modal-header">
											<h4 class="modal-title text-center">Restore your forgotten password</h4>
										</div>

										<div class="modal-body">
											<div class="row gap-20">
												
												<div class="col-sm-12 col-md-12">
													<p class="mb-20">Enter your email address and we'll send a link to change your password.</p>
												</div>
												
												<div class="col-sm-12 col-md-12">

													<div class="form-group"> 
														<label>Email Address</label>
														<input class="form-control" placeholder="Enter your email address" type="text"> 
													</div>
												
												</div>

												<div class="col-sm-12 col-md-12">
													<div class="checkbox-block"> 
														<input id="forgot_password_checkbox-2" name="forgot_password_checkbox-2" class="checkbox" value="First Choice" type="checkbox"> 
														<label class="" for="forgot_password_checkbox-2">Generate new password</label>
													</div>
												</div>
												
												<div class="col-sm-12 col-md-12">
													<div class="login-box-box-action">
														Return to <a href="login.html">Log-in</a>
													</div>
												</div>
												
											</div>
										</div>

										<div class="modal-footer text-center">
											<button type="button" class="btn btn-primary">Restore</button>
										</div>
										
									</div>

								</div>
							
							</div>
							
						</div>
						
					</div>

				</div>
			
			</div>



<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.client_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>