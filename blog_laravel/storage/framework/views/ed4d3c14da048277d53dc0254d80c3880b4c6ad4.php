<!DOCTYPE html>
<html lang="en">

<?php echo $__env->make('admin.template.admin_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class="app header-fixed left-sidebar-light left-sidebar-light-alt left-sidebar-fixed right-sidebar-fixed right-sidebar-overlay right-sidebar-hidden">
		<?php echo $__env->make('admin.template.admin_navbar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <!--===========app body start===========-->
    <div class="app-body">
      <?php echo $__env->make('admin.template.admin_sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--main contents start-->
        <main class="main-content">
			        <?php echo $__env->yieldContent('content'); ?>
        </main>
 
    </div>
     <?php echo $__env->make('admin.template.admin_footer', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    
	   <?php echo $__env->make('admin.template.admin_js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	   <?php echo $__env->yieldContent('custom_js'); ?>
</body>

</html>


 