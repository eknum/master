 

<?php $__env->startSection('content'); ?>

        <!--main contents start-->
        <main class="main-content">
            <!--page title start-->
            <div class="page-title">
                <h4 class="mb-0"> Add New Record
                </h4>
                <ol class="breadcrumb mb-0 pl-0 pt-1 pb-0">
                    <li class="breadcrumb-item"><a href="#" class="default-color">Home</a></li>
                    <li class="breadcrumb-item active">Basic Input</li>
                </ol>
            </div>
            <!--page title end-->


            <div class="container-fluid">

                <!-- state start-->
                <div class="row">
                    <div class=" col-md-6">
                       
                        
                        <div class="card card-shadow mb-4">
                            <div class="card-header">
                                <div class="card-title">
                                    Insert Data
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="post" action="<?php echo e(route('add')); ?>">
                                    <?php echo e(csrf_field()); ?>

                                    <input type="hidden" name="id" >
                                    <div class="form-group">
                                        <label for="name"><?php echo e(__('Name')); ?></label>
                                        <input type="text" name="name" class="form-control form-control-pill<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" id="name" placeholder="name" value="<?php echo e(old('name')); ?>" required autofocus>
                                         <?php if($errors->has('name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('name')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="email"><?php echo e(__('E-Mail Address')); ?></label>
                                        <input type="email" class="form-control form-control-pill<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" id="email" name="email" placeholder="email" value="<?php echo e(old('email')); ?>" required>
                                        <?php if($errors->has('email')): ?>
                                            <span class="invalid-feedback" role="alert">
                                                <strong><?php echo e($errors->first('email')); ?></strong>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                     <div class="form-group">
                                        <label for="inputPassword"><?php echo e(__('Password')); ?></label>
                                        <input type="password" class="form-control form-control-pill<?php echo e($errors->has('password') ? ' is-invalid' : ''); ?>" id="password" name="password" placeholder="password" required>
                                       <?php if($errors->has('password')): ?>
                                        <span class="invalid-feedback" role="alert">
                                            <strong><?php echo e($errors->first('password')); ?></strong>
                                        </span>
                                    <?php endif; ?>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword" class="sr-only"><?php echo e(__('Confirm Password')); ?></label>
                                        <input id="password-confirm" type="password" class="form-control form-control-pil" placeholder="confirm-password" name="password_confirmation" required>                
                                    </div>

                                    <button class="btn btn-lg btn-primary btn-block" name="edit" type="submit"><?php echo e(__('Add')); ?></button>
                                </form>

                            </div>
                        </div>

                    </div>
                   
                </div>

                <!-- state end-->

            </div>
        </main>
        <!--main contents end-->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('custom_js'); ?>
<script>
    $(document).ready(function() {
        $('#bs4-table').DataTable();
    } );
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>