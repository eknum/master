
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="MHS">

    <!--favicon icon-->
    <link rel="icon" type="image/png" href="<?php echo e(asset('public/assets3/img/favicon.html')); ?>">

    <title>Admin</title>

    <!--google font-->
    <link href="https://fonts.googleapis.com/css6079.css?family=Poppins:300,400,500,600,700" rel="stylesheet">


    <!--common style-->
    <link href="<?php echo e(asset('public/assets3/vendor/bootstrap/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/assets3/vendor/lobicard/css/lobicard.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/assets3/vendor/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/assets3/vendor/simple-line-icons/css/simple-line-icons.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/assets3/vendor/themify-icons/css/themify-icons.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/assets3/vendor/weather-icons/css/weather-icons.min.css')); ?>" rel="stylesheet">

    <!--easy pie chart-->
    <link href="<?php echo e(asset('public/assets3/vendor/jquery-easy-pie-chart/jquery.easy-pie-chart.css')); ?>" rel="stylesheet">

    <!--custom css-->
    <link href="<?php echo e(asset('public/assets3/css/main.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/assets3/css/custom.css')); ?>" rel="stylesheet">

    <!--bs4 data table-->
    <link href="<?php echo e(asset('public/assets3/vendor/data-tables/dataTables.bootstrap4.min.css')); ?>" rel="stylesheet">
    <!-- <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="assets/vendor/html5shiv.js"></script>
    <script src="assets/vendor/respond.min.js"></script>


    <![endif]-->
    <link href="<?php echo e(asset('public/assets3/css/automap.css')); ?>" rel="stylesheet">

   




   
</head>
 
