<?php $__env->startSection('content'); ?>
<div class="page-title">
      <h4 class="mb-0"> Menu  
      </h4>
</div>
<div class="container-fluid">
  <div class="row">
     <div class=" col-sm-12">
        <div class="card card-shadow mb-4">
            <?php echo $__env->make('admin.menu.tab', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
             <div class="tab-content">
          
                <div class="tab-pane active" id="tab-j_2" role="tabpanel">
              <div class="card-body">
                  <a href="<?php echo e(route('items.create')); ?>" class="btn btn-primary" style="float: right;"> Add Menu</a>&nbsp;
     
                    <table id="bs4-table" class="table table-bordered table-striped" cellspacing="0">

                         <thead>
                        <tr>                        
                            <th>Id</th>
                            <th>Item Name</th>
                            <th>Price</th>
                            <th>description</th>
                            <th>Menu</th>

                            <th>Action</th>
                        </tr>
                        </thead>
                        <?php $__currentLoopData = $item; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <tr>
                           <td><?php echo e($value->id); ?></td>
                           <td><?php echo e($value->name); ?></td>
                           <td><?php echo e($value->price); ?></td>
                           <td><?php echo e($value->description); ?></td>
                         <td><?php echo e($value->menu ? $value->menu->name : ''); ?></td> 

                           <td><a class="btn btn-primary" href="<?php echo e(route('items.edit',$value->id)); ?>"><i class="fa fa-fw fa-pencil"></i> Edit</a>&nbsp;
                                <a class="btn btn-danger" href="<?php echo e(route('items.delete',$value->id)); ?>" onclick="return confirm('are you sure you want to delete??')"><i class="fa fa-fw fa-trash-o"></i>Delete</a>
                           </td>
                       </tr>
                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                    </table>
                </div>
                                        
            </div>
          </div>

      </div>
  </div>
</div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('custom_js'); ?>
<script>
    $(document).ready(function() {
        $('#bs4-table').DataTable({
          "order": [[ 0, "desc" ]]
        });
    } );
</script>   
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.admin_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>