 

<?php $__env->startSection('content'); ?>

 <!--page title start-->
  <div class="page-title">
      <h4 class="mb-0"> Data Tables
          <small>start your new page</small>
      </h4>
  </div>
<div class="container-fluid">
  <!--page title end-->
  <div class="row">
      <div class=" col-sm-12">
          <div class="card card-shadow mb-4">
              <div class="card-header">
                  <div class="card-title">
                      Data Tables
                  </div>
              </div>
              <div class="card-body">
               <button><a href="<?php echo e(route('insert')); ?>"> Add</a></button>

                  <table id="bs4-table" class="table table-bordered table-striped" cellspacing="0">

                      <thead>
                      <tr>                        
                          <th>Id</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Type</th>
                          <th>Action</th>
                      </tr>
                      </thead>
                       <tbody>
                     
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr>
                            <td><?php echo e($value->id); ?></td>
                            <td><?php echo e($value->name); ?></td>
                            <td><?php echo e($value->email); ?></td>
                            <td><?php echo e($value->type); ?></td>
                            <td><a href="<?php echo e(route('edituser',$value->id)); ?>">Edit</a>
                              <a href="<?php echo e(route('deleteuser',$value->id)); ?>">Delete</a>
                            </td>

                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>          
                      </tbody>
                      <tfoot>
                      <tr>
                         <th>Id</th>
                          <th>Name</th>
                          <th>Email</th>
                          <th>Type</th>
                          <th>Action</th>
                      </tr>
                      </tfoot>
                     
                  </table>
              </div>
          </div>
      </div>
  </div>

</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('custom_js'); ?>
<script>
    $(document).ready(function() {
        $('#bs4-table').DataTable();
    } );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.admin_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>