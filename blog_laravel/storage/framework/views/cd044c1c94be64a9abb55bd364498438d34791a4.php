 

<?php $__env->startSection('wrapper'); ?>
<body class="not-transparent-header style-2">
		<!-- start Main Wrapper -->
		<div class="main-wrapper scrollspy-container">

			<div id="order-page-title" class="page-title-style2">

				<div class="container inner-img">
					<div class="order-top-section">
						<div class="Page-title">

							<div class="row single-restaurant-top-section">
							<form>
									<?php echo e(csrf_field()); ?>

								<div class="col-md-8">

									<div class="res-short-info">
										<h2> <?php echo e($restaurant->name); ?></h2>
										<ul class="list-unstyled">
											<!-- <li>
												<span class="glyphicon glyphicon-cutlery"></span>
												Cuisine Type:
											</li> -->
											<li><span class="fa fa-lock"></span> Min. Order <?php echo e($restaurant->minorder); ?></li>
											<div class="clearfix"></div>
										</ul>
									</div>
								</div>
							</form>
								<div class="col-md-4">
									<div class="restaurant-opening-info">

										<div class="res-rating-all">
											<ul class="list-unstyled">
												<li>
													<div class="res-opening-time"><i class="glyphicon glyphicon-time"></i>  <?php echo e($restaurant->open_time); ?> <?php echo e($restaurant->close_time); ?></div>
												</li>
												<li>
													<div class="ui-res-rating">
														<span class="glyphicon glyphicon-star" style="color: #E64D64;"></span>
														<span class="glyphicon glyphicon-star" style="color: #E64D64;"></span>
														<span class="glyphicon glyphicon-star" style="color: #E64D64;"></span>
														<span class="glyphicon glyphicon-star" style="color: #CCCCCC;"></span>
														<span class="glyphicon glyphicon-star" style="color: #CCCCCC;"></span>
													</div>
												</li>
												<li>
													<div class="ui-rating-text">
														2 Rating
													</div>
												</li>
												<div class="clearfix"></div>
											</ul>
										</div>
									</div>
								</div>


							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- start hero-header -->
			<div class="breadcrumb-wrapper">

				<div class="container">

					<ol class="breadcrumb-list booking-step">
						<li><a href="<?php echo e(route('client.index')); ?>">Home</a></li>
						<li><span>Order Online</span></li>
					</ol>

				</div>

			</div>
			<!-- end hero-header -->

			
			<div class="section sm">

				<!-- Content Start -->
				<!-- Start order-online-->
				<div id="order-online">
					<div class="container">


						<!-- Nested Row Starts -->
						<div class="row">
							<!-- Mainarea Starts -->
							<div class="col-md-9 col-xs-12">
								<!-- Menu Tabs Starts -->
								<div class="menu-tabs-wrap">
									<!-- Menu Tabs List Starts -->
									<ul class="nav nav-tabs nav-menu-tabs text-center-xs">
										<li class="active"><a href="#menu" data-toggle="tab">Menu</a></li>
										<li><a href="#offer" data-toggle="tab">Offer</a></li>
										<li><a href="#gallery" data-toggle="tab">Gallery</a></li>
										<li><a href="#reviews" data-toggle="tab">Reviews</a></li>
										<li><a href="#reachus" data-toggle="tab">Contact Us</a></li>
									</ul>
									<!-- Menu Tabs List Ends -->
									<!-- Menu Tabs Content Starts -->
									<div class="tab-content">
										<!-- Tab #1 Starts -->
										<div id="menu" class="tab-pane active">
											<!-- Tab #1 Nested Row Starts -->
											<div class="row">
												<!-- Left Column Starts -->
												<div class="col-sm-4 col-xs-12">
													<div class="side-block-1">
														<h6>Delivery Menu</h6>
														<ul class="list-unstyled list-style-2">
															<!-- Menus Items -->
															<li><i class="text-primary ion-android-checkmark-circle"></i><a href='order?item_id=-1'> All</a></li>
															<?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
															<li><i class="text-primary ion-android-checkmark-circle"></i><a href='order?item_id=<?php echo e($value->id); ?>'> <?php echo e($value->name); ?> </a></li>
															<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
														</ul>
													</div>

												</div>
												<!-- Left Column Ends -->
												<!-- Right Column Starts -->
												<div class="col-sm-8 col-xs-12">
													<!-- Order Menu Tab Pane Starts -->
													<div class="order-menu-tab-pane text-center-xs">
														<!-- Items List -->
														<div class="spacer"></div>
														<?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

														<div class="order-menu-item clearfix">
															<div class="pull-left">
																<h4> <?php echo e($value->name); ?></h4>
																<p></p>
															</div>
															<div class="pull-right">
																<a href="?orderID=<?php echo e($value->id); ?>" class="btn btn-primary animation"><span class="price-new"><?php echo e($value->price); ?></span> <i class="fa fa-plus-circle"></i></a>
															</div>
														</div>
														<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 
														<!-- Order Menu List #3 Ends -->
													</div>
													<!-- Order Menu Tab Pane Ends -->
													<!-- Pagination Starts -->
													<div class="pagination-block text-right text-center-xs mb-10">
														<ul class="pagination animation">
															<li><a href="#">&laquo;</a></li>
															<li class="active"><a href="#">1</a></li>
															<li><a href="#">2</a></li>
															<li><a href="#">3</a></li>
															<li><a href="#">&raquo;</a></li>
														</ul>
													</div>
													<!-- Pagination Ends -->
													<!-- Banners Starts -->
													<div class="row">
														<div class="col-xs-6">
															<img src="images/banners/banner-img1.png" alt="Banner 1" class="img-responsive img-center-xs">
														</div>
														<div class="col-xs-6">
															<img src="images/banners/banner-img2.png" alt="Banner 2" class="img-responsive img-center-xs">
														</div>
													</div>
													<!-- Banners Ends -->
												</div>
												<!-- Right Column Ends -->
											</div>
											<!-- Tab #1 Nested Row Ends -->
										</div>
										<!-- Tab #1 Ends -->
										<!-- Tab #2 Starts -->
										<div id="offer" class="tab-pane">
											<!-- Tab #2 Nested Row Starts -->
											<div class="row">
												<!-- Left Column Starts -->
												<div class="col-sm-4 col-xs-12">
													<div class="side-block-1">
														<h6>Delivery Menu</h6>
														<ul class="list-unstyled list-style-2">
															<li><i class="text-primary ion-android-checkmark-circle"></i> Top Kebab and Pizza</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Bombay Lancer</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Bombay Lancer</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Gillingham Grill</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Chinese Starters</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> North Indian Main Course</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Traditional Telugu Maincourse</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Indian Breads</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Rice, Biryani &amp; Pulao</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Accompaniments</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Desserts &amp; Beverages</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Flamers Pizza Ltd</li>
														</ul>
													</div>
												</div>
												<!-- Left Column Ends -->
												<!-- Right Column Starts -->
												<div class="col-sm-8 col-xs-12">
													<!-- Information Tab Pane Starts -->
													<div class="information-tab-pane">
														<p>
															<img src="images/banners/banner-discount.png" alt="Discount Banner" class="img-responsive">
														</p>
														<!-- Spacer Starts -->
														<div class="spacer big"></div>
														<!-- Spacer Ends -->
														<p>Thefoody is a nice restaurant offering Indian food to takeaway or eat in the restaurant. The restaurant offers Indian food takeaway.</p>
														<p>Free bottle fo white wine.</p>
														<p>Spend £39.95 or more</p>
														<p>Only available through our online ordering system on orders over £24.95 or more.</p>
														<hr>
														<!-- Opening Hours Starts -->
														<h5><i class="fa fa-clock-o"></i> Opening Hours</h5>
														<ul class="list-unstyled timing-list">
															<li class="clearfix">
																<span class="pull-left">Monday</span>
																<span class="pull-right text-right">12:00 - 15:30, 18:00 - 22:30</span>
															</li>
															<li class="clearfix">
																<span class="pull-left">Tuesday</span>
																<span class="pull-right text-right">12:00 - 15:30, 18:00 - 22:30</span>
															</li>
															<li class="clearfix">
																<span class="pull-left">Wednesday</span>
																<span class="pull-right text-right">12:00 - 15:30, 18:00 - 22:30</span>
															</li>
															<li class="clearfix">
																<span class="pull-left">Thursday</span>
																<span class="pull-right text-right">12:00 - 15:30, 18:00 - 22:30</span>
															</li>
															<li class="clearfix">
																<span class="pull-left">Friday</span>
																<span class="pull-right text-right">12:00 - 15:30, 18:00 - 22:30</span>
															</li>
															<li class="clearfix">
																<span class="pull-left">Saturday</span>
																<span class="pull-right text-right">12:00 - 15:30, 18:00 - 22:30</span>
															</li>
															<li class="clearfix">
																<span class="pull-left">Sunday</span>
																<span class="pull-right text-right">12:00 - 15:30, 18:00 - 22:30</span>
															</li>
														</ul>
														<!-- Takeway Hours Ends -->
														<hr>
														<!-- Spacer Starts -->
														<div class="spacer"></div>
														<!-- Spacer Ends -->
														<!-- Banners Starts -->
														<div class="row">
															<div class="col-xs-6">
																<img src="images/banners/banner-img1.png" alt="Banner 1" class="img-responsive img-center-xs">
															</div>
															<div class="col-xs-6">
																<img src="images/banners/banner-img2.png" alt="Banner 2" class="img-responsive img-center-xs">
															</div>
														</div>
														<!-- Banners Ends -->
													</div>
													<!-- Information Tab Pane Ends -->
												</div>
												<!-- Right Column Ends -->
											</div>
											<!-- Tab #1 Nested Row Ends -->
										</div>
										<!-- Tab #2 Ends -->
										<!-- Tab #3 Starts -->
										<div id="gallery" class="tab-pane">
											<!-- Image Gallery Starts -->
											<ul class="row list-unstyled gallery-grid">
												<!-- Gallery Image #1 Starts -->
												<li class="col-sm-4 col-xs-6">
													<div class="hover-content">
														<img src="images/portfolio/portfolio01.png" alt="Gallery Image" class="img-responsive img-center-sm img-center-xs">
														<div class="overlay animation">
															<a href="images/portfolio/portfolio01.png" class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
														</div>
													</div>
												</li>
												<!-- Gallery Image #1 Ends -->
												<!-- Gallery Image #2 Starts -->
												<li class="col-sm-4 col-xs-6">
													<div class="hover-content">
														<img src="images/portfolio/portfolio02.png" alt="Gallery Image" class="img-responsive img-center-sm img-center-xs">
														<div class="overlay animation">
															<a href="images/portfolio/portfolio02.png" class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
														</div>
													</div>
												</li>
												<!-- Gallery Image #2 Ends -->
												<!-- Gallery Image #3 Starts -->
												<li class="col-sm-4 col-xs-6">
													<div class="hover-content">
														<img src="images/portfolio/portfolio03.png" alt="Gallery Image" class="img-responsive img-center-sm img-center-xs">
														<div class="overlay animation">
															<a href="images/portfolio/portfolio03.png" class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
														</div>
													</div>
												</li>
												<!-- Gallery Image #3 Ends -->
												<!-- Gallery Image #4 Starts -->
												<li class="col-sm-4 col-xs-6">
													<div class="hover-content">
														<img src="images/portfolio/portfolio04.png" alt="Gallery Image" class="img-responsive img-center-sm img-center-xs">
														<div class="overlay animation">
															<a href="images/portfolio/portfolio04.png" class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
														</div>
													</div>
												</li>
												<!-- Gallery Image #4 Ends -->
												<!-- Gallery Image #5 Starts -->
												<li class="col-sm-4 col-xs-6">
													<div class="hover-content">
														<img src="images/portfolio/portfolio05.png" alt="Gallery Image" class="img-responsive img-center-sm img-center-xs">
														<div class="overlay animation">
															<a href="images/portfolio/portfolio05.png" class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
														</div>
													</div>
												</li>
												<!-- Gallery Image #5 Ends -->
												<!-- Gallery Image #6 Starts -->
												<li class="col-sm-4 col-xs-6">
													<div class="hover-content">
														<img src="images/portfolio/portfolio06.png" alt="Gallery Image" class="img-responsive img-center-sm img-center-xs">
														<div class="overlay animation">
															<a href="images/portfolio/portfolio06.png" class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
														</div>
													</div>
												</li>
												<!-- Gallery Image #6 Ends -->
												<!-- Gallery Image #7 Starts -->
												<li class="col-sm-4 col-xs-6">
													<div class="hover-content">
														<img src="images/portfolio/portfolio07.png" alt="Gallery Image" class="img-responsive img-center-sm img-center-xs">
														<div class="overlay animation">
															<a href="images/portfolio/portfolio07.png" class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
														</div>
													</div>
												</li>
												<!-- Gallery Image #7 Ends -->
												<!-- Gallery Image #8 Starts -->
												<li class="col-sm-4 col-xs-6">
													<div class="hover-content">
														<img src="images/portfolio/portfolio08.png" alt="Gallery Image" class="img-responsive img-center-sm img-center-xs">
														<div class="overlay animation">
															<a href="images/portfolio/portfolio08.png" class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
														</div>
													</div>
												</li>
												<!-- Gallery Image #8 Ends -->
												<!-- Gallery Image #9 Starts -->
												<li class="col-sm-4 col-xs-6">
													<div class="hover-content">
														<img src="images/portfolio/portfolio02.png" alt="Gallery Image" class="img-responsive img-center-sm img-center-xs">
														<div class="overlay animation">
															<a href="images/portfolio/portfolio02.png" class="btn btn-link zoom"><i class="fa fa-search-plus"></i></a>
														</div>
													</div>
												</li>
												<!-- Gallery Image #9 Ends -->
											</ul>
											<!-- Image Gallery Ends -->
										</div>
										<!-- Tab #3 Ends -->
										<!-- Tab #4 Starts -->
										<div id="reviews" class="tab-pane">
											<!-- Tab #4 Nested Row Starts -->
											<div class="row">
												<!-- Left Column Starts -->
												<div class="col-sm-4 col-xs-12">
													<div class="side-block-1">
														<h6>Delivery Menu</h6>
														<ul class="list-unstyled list-style-2">
															<li><i class="text-primary ion-android-checkmark-circle"></i> Top Kebab and Pizza</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Bombay Lancer</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Bombay Lancer</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Gillingham Grill</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Chinese Starters</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> North Indian Main Course</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Traditional Telugu Maincourse</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Indian Breads</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Rice, Biryani &amp; Pulao</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Accompaniments</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Desserts &amp; Beverages</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Flamers Pizza Ltd</li>
														</ul>
													</div>
												</div>
												<!-- Left Column Ends -->
												<!-- Right Column Starts -->
												<div class="col-sm-8 col-xs-12">
													<!-- Reviews Tab Pane Starts -->
													<div class="reviews-tab-pane">
														<!-- Reviews Form Box Starts -->
														<div class="reviews-form-box">
															<h6>Write a Review</h6>
															<form method="post" action="#">
                                    						<?php echo e(csrf_field()); ?>


																<textarea class="form-control" rows="4" placeholder="Message should be atleast 130 charecters" name="review" ></textarea>
																<div class="clearfix">
																	<ul class="list-unstyled list-inline rating-star pull-left">
																		<li><i class="fa fa-star"></i></li>
																		<li><i class="fa fa-star"></i></li>
																		<li><i class="fa fa-star"></i></li>
																		<li><i class="fa fa-star"></i></li>
																		<li><i class="fa fa-star"></i></li>
																	</ul>
																
																	<button>submit</button>
																</div>
															</form>
														</div>
														<!-- Reviews Form Box Ends -->
														<!-- Reviews List Starts -->
														<div class="reviews-box">
															<!-- Review #1 Starts -->
															
															<div class="review-list">
																<div class="clearfix">
																	<div class="pull-left">
																		<h6><i class="fa fa-calendar"></i> Mar 10, 2016</h6>
																		<h6>By Sainath Chillapuram</h6>
																		<ul class="list-unstyled list-inline rating-star-list">
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star-o"></i></li>
																			<li><i class="fa fa-star-o"></i></li>
																		</ul>
																	</div>
																	<img src="images/review-thumb-img1.png" alt="Image" class="img-responsive pull-right">
																</div>
																<div class="review-list-content">
																	
																</div>
															</div>
															
															<!-- Review #1 Ends -->
															<!-- Review #2 Starts -->
															<div class="review-list">
																<div class="clearfix">
																	<div class="pull-left">
																		<h6><i class="fa fa-calendar"></i> Mar 10, 2016</h6>
																		<h6>By Sainath Chillapuram</h6>
																		<ul class="list-unstyled list-inline rating-star-list">
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star-o"></i></li>
																			<li><i class="fa fa-star-o"></i></li>
																		</ul>
																	</div>
																	<img src="images/review-thumb-img1.png" alt="Image" class="img-responsive pull-right">
																</div>
																<div class="review-list-content">
																	<p>The delivery guy said he will be back in 10 mins with the missing items, but never came back. when I called the restaurant, they took another 30 mins to deliver.</p>
																</div>
															</div>
															<!-- Review #2 Ends -->
															<!-- Review #3 Starts -->
															<div class="review-list">
																<div class="clearfix">
																	<div class="pull-left">
																		<h6><i class="fa fa-calendar"></i> Mar 10, 2016</h6>
																		<h6>By Sainath Chillapuram</h6>
																		<ul class="list-unstyled list-inline rating-star-list">
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star-o"></i></li>
																			<li><i class="fa fa-star-o"></i></li>
																		</ul>
																	</div>
																	<img src="images/review-thumb-img1.png" alt="Image" class="img-responsive pull-right">
																</div>
																<div class="review-list-content">
																	<p>The delivery guy said he will be back in 10 mins with the missing items, but never came back. when I called the restaurant, they took another 30 mins to deliver.</p>
																</div>
															</div>
															<!-- Review #3 Ends -->
															<!-- Review #4 Starts -->
															<div class="review-list">
																<div class="clearfix">
																	<div class="pull-left">
																		<h6><i class="fa fa-calendar"></i> Mar 10, 2016</h6>
																		<h6>By Sainath Chillapuram</h6>
																		<ul class="list-unstyled list-inline rating-star-list">
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star"></i></li>
																			<li><i class="fa fa-star-o"></i></li>
																			<li><i class="fa fa-star-o"></i></li>
																		</ul>
																	</div>
																	<img src="images/review-thumb-img1.png" alt="Image" class="img-responsive pull-right">
																</div>
																<div class="review-list-content">
																	<p>The delivery guy said he will be back in 10 mins with the missing items, but never came back. when I called the restaurant, they took another 30 mins to deliver.</p>
																</div>
															</div>
															<!-- Review #4 Ends -->
														</div>
														<!-- Reviews List Ends -->
														<!-- Spacer Starts -->
														<div class="spacer-1 condensed"></div>
														<!-- Spacer Ends -->
														<!-- Banners Starts -->
														<div class="row">
															<div class="col-xs-6">
																<img src="images/banners/banner-img1.png" alt="Banner 1" class="img-responsive img-center-xs">
															</div>
															<div class="col-xs-6">
																<img src="images/banners/banner-img2.png" alt="Banner 2" class="img-responsive img-center-xs">
															</div>
														</div>
														<!-- Banners Ends -->
													</div>
													<!-- Reviews Tab Pane Ends -->
												</div>
												<!-- Right Column Ends -->
											</div>
											<!-- Tab #4 Nested Row Ends -->
										</div>
										<!-- Tab #4 Ends -->
										<!-- Tab #5 Starts -->
										<div id="reachus" class="tab-pane">
											<!-- Tab #5 Nested Row Starts -->
											<div class="row">
												<!-- Left Column Starts -->
												<div class="col-sm-4 col-xs-12">
													<div class="side-block-1">
														<h6>Delivery Menu</h6>
														<ul class="list-unstyled list-style-2">
															<li><i class="text-primary ion-android-checkmark-circle"></i> Top Kebab and Pizza</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Bombay Lancer</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Bombay Lancer</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Gillingham Grill</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Chinese Starters</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> North Indian Main Course</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Traditional Telugu Maincourse</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Indian Breads</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Rice, Biryani &amp; Pulao</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Accompaniments</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Desserts &amp; Beverages</li>
															<li><i class="text-primary ion-android-checkmark-circle"></i> Flamers Pizza Ltd</li>
														</ul>
													</div>
												</div>
												<!-- Left Column Ends -->
												<!-- Right Column Starts -->
												<div class="col-sm-8 col-xs-12">
													<!-- Reach Us Tab Pane Starts -->
													<div class="reachus-tab-pane">
														<!-- Map Starts -->
														<div class="map"></div>
														<!-- Map Ends -->
														<!-- Address Block Starts -->
														<div class="address-block">
															<h5><i class="fa fa-tag"></i> Address</h5>
															<ul class="list-unstyled">
																<li> # 3453 corn street, Sanford, FL 34232.,</li>
																<li>Cardiff, London.</li>
															</ul>
															<h5><i class="fa fa-phone"></i> Phone Numbers</h5>
															<ul class="list-unstyled">
																<li>+0 (123) 456 - 7890,  +0 (123) 456 - 7890</li>
															</ul>
															<h5><i class="fa fa-mobile"></i> Can be in touch with Watsapp</h5>
															<ul class="list-unstyled">
																<li>+0 (123) 456 - 7890</li>
															</ul>
														</div>
														<!-- Address Block Ends -->
														<!-- Reach Form Starts -->
														<div class="reachus-form">
															<h5>Drop us a mail for Table Reservation</h5>
															<form action="#">
																<div class="form-group">
																	<label class="sr-only" for="reachus-name">Name</label>
																	<input type="text" class="form-control flat" id="reachus-name" placeholder="Name">
																</div>
																<div class="form-group">
																	<label class="sr-only" for="reachus-email">E-mail</label>
																	<input type="email" class="form-control flat" id="reachus-email" placeholder="e-mail">
																</div>
																<div class="form-group">
																	<label class="sr-only" for="reachus-mobileno">Mobile No</label>
																	<input type="text" class="form-control flat" id="reachus-mobileno" placeholder="Mobile No">
																</div>
																<div class="row">
																	<div class="col-xs-6">
																		<div class="form-group">
																			<div class="input-group">
																				<label class="sr-only" for="reachus-date">Date</label>
																				<input type="text" class="form-control flat datepickerInput" id="reachus-date" placeholder="Date">
																	<span class="input-group-addon flat">
																		<span class="fa fa-calendar"></span>
																	</span>
																			</div>
																		</div>
																	</div>
																	<div class="col-xs-6">
																		<div class="form-group">
																			<label class="sr-only">No.of Persons</label>
																			<select class="form-control flat">
																				<option>No.of Persons</option>
																				<option>1</option>
																				<option>2</option>
																				<option>3</option>
																				<option>4</option>
																				<option>5</option>
																			</select>
																		</div>
																	</div>
																</div>
																<div class="form-group">
																	<label class="sr-only" for="reachus-info">Your Information</label>
																	<textarea class="form-control flat" id="reachus-info" placeholder="Your Information" rows="5"></textarea>
																</div>
																<div class="clearfix">
																	<button type="submit" class="btn btn-primary mt-5 animation pull-right">Submit</button>
																</div>
															</form>
														</div>
														<!-- Reach Form Ends -->
														<!-- Banners Starts -->
														<div class="row">
															<div class="col-xs-6">
																<img src="images/banners/banner-img1.png" alt="Banner 1" class="img-responsive img-center-xs">
															</div>
															<div class="col-xs-6">
																<img src="images/banners/banner-img2.png" alt="Banner 2" class="img-responsive img-center-xs">
															</div>
														</div>
														<!-- Banners Ends -->
													</div>
													<!-- Reach Us Tab Pane Ends -->
												</div>
												<!-- Right Column Ends -->
											</div>
											<!-- Tab #5 Nested Row Ends -->
										</div>
										<!-- Tab #5 Ends -->
									</div>
									<!-- Menu Tabs Content Ends -->
								</div>
								<!-- Menu Tabs Ends -->
							</div>
							<!-- Mainarea Ends -->
							<!-- Sidearea Starts -->
							<div class="col-md-3 col-xs-12">
								<!-- Spacer Starts -->
								<div class="spacer-1 medium hidden-lg hidden-md"></div>
								<!-- Spacer Ends -->
								<!-- Your Order Starts -->
								<form method="post" action="">
									<?php echo e(csrf_field()); ?>

								<div class="side-block-order border-radius-4">
									<!-- Heading Starts -->
									<h5 class="text-center"><i class="fa fa-shopping-basket"></i> Your Orders</h5>
									<!-- Heading Ends -->
									<!-- Order Content Starts -->

									<div class="side-block-order-content">
										<!-- Order Item List Starts -->
										<ul class="list-unstyled order-item-list">
											<?php $__currentLoopData = $cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
											<li class="clearfix">
												<div class="pull-left">
													<div class="update-product">
														<a title="Add a product" href="?cartpId=<?php echo e($value->id); ?>&quantity=<?php echo e($value->quantity); ?>"><i class="fa fa-plus-circle"></i></a>
														<a title="Minus a product" href="?cartmId=<?php echo e($value->id); ?>&quantity=<?php echo e($value->quantity); ?>"><i class="fa fa-minus-circle"></i></a>
													</div>
												</div>
												<div class="cart-product-name pull-left"><?php echo e($value->name); ?></div>
												<div class="cart-product-price pull-right text-spl-color"><?php echo e($value->quantity); ?> x <?php echo e($value->price); ?> = <?php echo e($value->quantity*$value->price); ?></div>
												<div class="cart-product-remove"><a title="Remove a product" href="?cartRId=<?php echo e($value->id); ?>"><i class="fa fa-trash"></i></a></div>

											</li>
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											
										<!-- Order Item List Ends -->
										<!-- Order Item Total Starts -->
										
										<dl class="dl-horizontal order-item-total">
											<dt class="text-light">Orders Amount :</dt>
											<dd class="text-spl-color text-right">$30.45</dd>
											<dt class="text-light">Delivery Charges :</dt>
											<dd class="text-spl-color text-right">$5.00</dd>
											<hr>
											<dt class="text-bold">Total  Amount :</dt>
											<dd class="text-bold text-spl-color text-right">$35.45</dd>
										</dl>
										<!-- Order Item Total Ends -->
										<div class="cfo-checkoutarea">
											<button type="submit" id="a" name="a" class="btn btn-primary btn-block custom-checkout">Proceed to Checkout</button>
										</div>
										
									</div>
									<!-- Order Content Ends -->
								</div>
								<!-- Your Order Ends -->
								</form>

								<!-- start add wrapper -->
								<div id="ad-wrapper">

									<img class="img-responsive" src="images/add-banner/add-banner.png" alt="">

								</div>
								<!-- end add wrapper -->

							</div>
							<!-- Sidearea Ends -->
						</div>
						<!-- Nested Row Ends -->
					</div>
					<!--.container-->
				</div>
				<!--#order-online-->
				<!--end order-online-->
				<!-- Content End -->

				<!-- start mobile footer nav -->
				<nav class="navbar navbar-default navbar-fixed-bottom visible-xs visible-sm mobile-cart-nav">
					<div class="mobile-cart-inner-content">
						<div class="row">
							<div class="col-md-4 col-xs-4">
								<div class="mobile-cart-item">
									<a id="mobileCartToggle" href="#"><i class="fa fa-shopping-basket"></i><span id="cart-item"> 10</span></a>
								</div>
							</div>
							<div class="col-md-4 col-xs-4">
								<div class="mobile-total-amount">Total: &pound;<span id="total-cart-amount">50</span></div>
							</div>
							<div class="col-md-4 col-xs-4">
								<a href="#" class="btn mobile-btn-checkout">Checkout</a>
							</div>
						</div>
					</div>
				</nav>
				<!-- end mobile footer nav -->
			
			</div>

			<!-- start footer -->
	
			
			<!-- end footer -->
			
		</div>
		<!-- end Main Wrapper -->
	</div> <!-- / .wrapper -->
	<!-- end Container Wrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('client.client_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>