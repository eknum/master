    <!-- Placed js at the end of the page so the pages load faster -->
    <script src="<?php echo e(asset('public/assets3/vendor/jquery/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/jquery-ui-1.12.1/jquery-ui.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/popper.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/bootstrap/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/jquery-ui-touch/jquery.ui.touch-punch-improved.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/lobicard/js/lobicard.js')); ?>"></script>
    <script class="include" type="text/javascript" src="<?php echo e(asset('public/assets3/vendor/jquery.dcjqaccordion.2.7.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/jquery.scrollTo.min.js')); ?>"></script>

    <!--sparkline chart-->
    <script src="<?php echo e(asset('public/assets3/vendor/sparkline/jquery.sparkline.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/sparkline/sparkline-init.js')); ?>"></script>

    <!--easy pie chart-->
    <script src="<?php echo e(asset('public/assets3/vendor/jquery-easy-pie-chart/jquery.easy-pie-chart.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/jquery-easy-pie-chart/easy-pie-chart-init.js')); ?>"></script>

    <!--Morris Chart-->
    <script src="<?php echo e(asset('public/assets3/vendor/morris-chart/morris.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/morris-chart/raphael-min.js')); ?>"></script>
        <!--datatables-->
    <script src="<?php echo e(asset('public/assets3/vendor/data-tables/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/vendor/data-tables/dataTables.bootstrap4.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/assets3/js/scripts.js')); ?>"></script>


    <!-- <script src="<?php echo e(asset('public/assets3/js/mapautoplaces.js')); ?>"></script> -->

   

    <script>

        // donut chart

        Morris.Donut({
            element: 'donut-chart',
            data: [
                {value: 60, label: 'Apple', formatted: 'at least 55%' },
                {value: 25, label: 'Orange', formatted: 'approx. 25%' },
                {value: 5, label: 'Banana', formatted: 'approx. 10%' },
                {value: 10, label: 'Long title chart', formatted: 'at most 10%' }
            ],
            backgroundColor: '#fff',
            labelColor: '#53505F',
            gridLineColor: '#e5ebf8',
            colors: [
                '#A768F3','#36a2f5','#34bfa3','#eac459'
            ],
            formatter: function (x, data) { return data.formatted; }
        });

    </script>

    <!--chartjs-->
    <script src="<?php echo e(asset('public/assets3/vendor/chartjs/Chart.min.js')); ?>"></script>
    <!--chartjs init-->
    <script>
        var ctx = document.getElementById('myChart3-light').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q"],
                datasets: [{
                    label: '# of Votes',
                    data: [58, 80, 44, 76, 54, 50, 45, 90, 57, 48, 54, 49, 63, 77, 67, 83, 95],
                    backgroundColor: [
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5',
                        '#36a2f5'
                    ],
                    //borderColor: [
                    //    'rgba(255,99,132,1)',
                    //    'rgba(54, 162, 235, 1)',
                    //    'rgba(255, 206, 86, 1)',
                    //    'rgba(75, 192, 192, 1)',
                    //    'rgba(153, 102, 255, 1)',
                    //    'rgba(255, 159, 64, 1)'
                    //],
                    borderWidth: 0
                }]
            },
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        display: false
                    }],
                    yAxes: [{
                        display: false
                    }]
                }

            }
        });


        var ctx = document.getElementById('myChart4-light').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "My First dataset",
                    backgroundColor: 'rgb(255,255,255,0)',
                    //backgroundColor: 'rgba(167,104,243,.2)',
                    borderColor: 'rgba(255,81,138,1)',
                    data: [6.06, 82.2, -22.11, 21.53, -21.47, 73.61, -53.75, -60.32]
                }]


            },

            // Configuration options go here
            options: {
                maintainAspectRatio: false,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            color: 'transparent',
                            zeroLineColor: 'transparent'
                        },
                        ticks: {
                            fontSize: 2,
                            fontColor: 'transparent'
                        }

                    }],
                    yAxes: [{
                        display: false,
                        ticks: {
                            display: false
                            //min: Math.min.apply(Math, data.datasets[0].data) - 5,
                            //max: Math.max.apply(Math, data.datasets[0].data) + 5
                        }
                    }]
                },
                elements: {
                    line: {
                        tension: 0.00001,
                        //tension: 0.4,
                        borderWidth: 1
                    },
                    point: {
                        radius: 4,
                        hitRadius: 10,
                        hoverRadius: 4
                    }
                }
            }
        });

      
    </script>
    <!-- places map search -->
    
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDR4mDK5ZcSfW8dhMJaoSAk9IMO-RxJXz8&libraries=places"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', function () {
        var places = new google.maps.places.Autocomplete(document.getElementById('txtPlaces'));
        google.maps.event.addListener(places, 'place_changed', function () {

        });
    });
</script>   
 

  
    
