 

<?php $__env->startSection('content'); ?>

    

        <!--main contents start-->
            <!--page title start-->
            <div class="page-title">
                <h4 class="mb-0"> Modal
                    <small>different Modal style</small>
                </h4>
                <ol class="breadcrumb mb-0 pl-0 pt-1 pb-0">
                    <li class="breadcrumb-item"><a href="#" class="default-color">Home</a></li>
                    <li class="breadcrumb-item active">Modal</li>
                </ol>
            </div>
            <!--page title end-->


            <div class="container-fluid">

                <!-- state start-->
                <div class="row">
                    <div class=" col-sm-4">
                        <div class="card card-shadow mb-4">
                          
                            <div class="card-header">
                                <div class="card-title">
                                    Default
                                </div>
                            </div>
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <input type="text" name="copyright" value="">

                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                    Launch demo modal
                                </button>

                                <!-- Modal -->
                                
                            </div>
                        </div>
                    </div>

                    <div class=" col-sm-4">
                        <div class="card card-shadow mb-4">
                            <div class="card-header">
                                <div class="card-title">
                                    Scrolling long content
                                </div>
                            </div>
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModalLong">
                                    Launch demo modal
                                </button>

                               
                            </div>
                        </div>
                    </div>

                    <div class=" col-sm-4">
                        <div class="card card-shadow mb-4">
                            <div class="card-header">
                                <div class="card-title">
                                    Tooltips and popovers
                                </div>
                            </div>
                            <div class="card-body">
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal2">
                                    Launch demo modal
                                </button>


                            </div>
                        </div>
                    </div>

                </div>
                <!-- state end-->

            </div>
        <!--main contents end-->

  <?php $__env->stopSection(); ?>
<?php $__env->startSection('custom_js'); ?>
<script>
    $(document).ready(function() {
        $('#bs4-table').DataTable({
          "order": [[ 0, "desc" ]]
        });
    } );
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.admin_theme', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>