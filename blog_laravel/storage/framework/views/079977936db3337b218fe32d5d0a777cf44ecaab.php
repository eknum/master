
<!DOCTYPE html>

<html lang="en">
<?php echo $__env->make('admin.template.admin_css', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<body class="login_page" >

 <!--===========login start===========-->

    <div class="container">
        <div class="card-header"><?php echo e(__('Reset Password')); ?></div>


        <?php if(session('status')): ?>
          <div class="alert alert-success" role="alert">
              <?php echo e(session('status')); ?>

           </div>
         <?php endif; ?>
        <form method="POST" class="form-signin" action="<?php echo e(route('password.email')); ?>">
                        <?php echo csrf_field(); ?>
            <a href="index.html" class="brand text-center">
                <img src="<?php echo e(asset('public/assets3/img/logo-dark.png')); ?>" srcset="assets/img/logo-dark@2x.png 2x" alt=""/>
            </a>
            <hr>
            <h2 class="form-signin-heading">Reset Password</h2>
            <div class="form-group">
                 <label for="email" class="sr-only"><?php echo e(__('E-Mail Address')); ?></label>
                 <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" placeholder="email" value="<?php echo e(old('email')); ?>" required>
                  <?php if($errors->has('email')): ?>
                    <span class="invalid-feedback" role="alert">
                        <strong><?php echo e($errors->first('email')); ?></strong>
                    </span>
                <?php endif; ?>
                
            </div>

            
            
            <button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo e(__('Send Password Reset Link')); ?></button>

        
        </form>

    </div>
    <!--===========login end===========-->
    <?php echo $__env->make('admin.template.admin_js', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>